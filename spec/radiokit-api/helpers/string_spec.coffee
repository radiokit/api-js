"use strict"

RadioKit = require("../../../lib/radiokit-api")

describe "RadioKit.Helpers.String", ->
  describe ".underscore", ->
    describe "if no argument is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Helpers.String.underscore()
        .toThrow()

    describe "if argument other than string is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Helpers.String.underscore(123)
        .toThrow()

    describe "if string is passed as an argument", ->
      it "does not throw an error", ->
        expect ->
          new RadioKit.Helpers.String.underscore("AbcdEfgh")
        .not.toThrow()

      describe "and it does not contain ::", ->
        describe "and it is all lowercase", ->
          it "returns it unmodified", ->
            expect(RadioKit.Helpers.String.underscore("abcd")).toEqual("abcd")

        describe "and it is all uppercase", ->
          it "returns lowercase string separated by underscores treating each letter as indicator to put underscore", ->
            expect(RadioKit.Helpers.String.underscore("ABCD")).toEqual("a_b_c_d")

        describe "and it it camelCase", ->
          it "returns lowercase string separated by underscores putting underscores before each uppercase letter", ->
            expect(RadioKit.Helpers.String.underscore("camelCase")).toEqual("camel_case")

        describe "and it it CamelCase", ->
          it "returns lowercase string separated by underscores putting underscores before each uppercase letter", ->
            expect(RadioKit.Helpers.String.underscore("CamelCase")).toEqual("camel_case")

      describe "and it contains ::", ->
        describe "and it is all lowercase", ->
          it "returns it unmodified with :: replaced with /", ->
            expect(RadioKit.Helpers.String.underscore("ab::cd")).toEqual("ab/cd")

        describe "and it is all uppercase", ->
          it "returns lowercase string separated by underscores treating each letter as indicator to put underscore with :: replaced with /", ->
            expect(RadioKit.Helpers.String.underscore("AB::CD")).toEqual("a_b/c_d")

        describe "and it it camelCase", ->
          it "returns lowercase string separated by underscores putting underscores before each uppercase letter with :: replaced with /", ->
            expect(RadioKit.Helpers.String.underscore("camelCase::AgainCase")).toEqual("camel_case/again_case")

        describe "and it it CamelCase", ->
          it "returns lowercase string separated by underscores putting underscores before each uppercase letter with :: replaced with /", ->
            expect(RadioKit.Helpers.String.underscore("CamelCase::AgainCase")).toEqual("camel_case/again_case")
