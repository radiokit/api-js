"use strict"

RadioKit = require("../../../lib/radiokit-api")

describe "RadioKit.Helpers.Location", ->
  describe ".parseHash", ->
    describe "if no argument is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Helpers.Location.parseHash()
        .toThrow()

    describe "if argument other than string is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Helpers.Location.parseHash(123)
        .toThrow()

    describe "if string is passed as an argument", ->
      it "does not throw an error", ->
        expect ->
          new RadioKit.Helpers.Location.parseHash("AbcdEfgh")
        .not.toThrow()

      describe "and it is empty", ->
        it "returns an empty object", ->
          expect(RadioKit.Helpers.Location.parseHash("")).toEqual({})

      describe "and it is just #", ->
        it "returns an empty object", ->
          expect(RadioKit.Helpers.Location.parseHash("#")).toEqual({})

      describe "and it contains key=value", ->
        it "returns an object that contains this key and value", ->
          expect(RadioKit.Helpers.Location.parseHash("key=value")).toEqual({key: "value"})

      describe "and it contains #key=value", ->
        it "returns an object that contains this key and value", ->
          expect(RadioKit.Helpers.Location.parseHash("#key=value")).toEqual({key: "value"})

      describe "and it contains #key=uri_encoded_value", ->
        it "returns an object that contains this key and value", ->
          expect(RadioKit.Helpers.Location.parseHash("#key=29409487279510405%7C%2F")).toEqual({key: "29409487279510405|/"})

      describe "and it contains key1=value1&key2=value2", ->
        it "returns an object that contains this key and value", ->
          expect(RadioKit.Helpers.Location.parseHash("key1=value1&key2=value2")).toEqual({key1: "value1", key2: "value2"})

      describe "and it contains #key1=value1&key2=value2", ->
        it "returns an object that contains this key and value", ->
          expect(RadioKit.Helpers.Location.parseHash("#key1=value1&key2=value2")).toEqual({key1: "value1", key2: "value2"})
