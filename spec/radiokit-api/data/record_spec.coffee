"use strict"

RadioKit = require("../../../lib/radiokit-api")

describe "RadioKit.Data.Record", ->
  successfulAuth = new RadioKit.Auth.Editor.OAuth2({ clientId: "abc", redirectUrl: "def" })

  describe "constructor", ->
    describe "if no argument is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Data.Record()
        .toThrowError(TypeError, "Missing argument options in RadioKitDataRecord constructor")

    describe "if argument other than object is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Data.Record("abcd")
        .toThrowError(TypeError, "Options passed to RadioKitDataRecord must be an object, string given")

    describe "if object is passed as an argument", ->
      describe "but auth validations are not passed", ->
        describe "it does not contain auth key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ model: "Track", appName: "abc", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'auth' key that is a object, no or undefined value was passed")

        describe "it does contain auth key but it is a string", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ model: "Track", appName: "abc", appConfig: {}, auth: "Abc" })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'auth' key that is a object, string given")

        describe "it does contain auth key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ model: "Track", appName: "abc", appConfig: {}, auth: 123 })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'auth' key that is a object, number given")

      describe "but appName validations are not passed", ->
        describe "it does not contain appName key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, model: "Track", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'appName' key that is a string, no or undefined value was passed")

        describe "it does contain appName key but it is an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, model: "Track", appName: {}, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'appName' key that is a string, object given")

        describe "it does contain appName key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, model: "Track", appName: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'appName' key that is a string, number given")

      describe "but appConfig validations are not passed", ->
        describe "it does not contain appConfig key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ model: "Track", appName: "abc", auth: successfulAuth })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'appConfig' key that is a object, no or undefined value was passed")

        describe "it does contain appConfig key but it is not an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ model: "Track", appName: "abc", auth: successfulAuth, appConfig: "Abc" })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'appConfig' key that is a object, string given")

        describe "it does contain appConfig key but it is not a an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ model: "Track", appName: "abc", auth: successfulAuth, appConfig: 123 })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'appConfig' key that is a object, number given")

      describe "but model validations are not passed", ->
        describe "it does not contain model key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'model' key that is a string, no or undefined value was passed")

        describe "it does contain model key but it is an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: {}, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'model' key that is a string, object given")

        describe "it does contain model key but it is not a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'model' key that is a string, number given")


      describe "but apiVersion validations are not passed", ->
        describe "it does contain apiVersion key but it is an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: "MyModel", apiVersion: {}, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'apiVersion' key that is a string, object given")

        describe "it does contain apiVersion key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: "MyModel", apiVersion: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'apiVersion' key that is a string, number given")


      describe "but recordId validations are not passed", ->
        describe "it does contain recordId key but it is an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: "MyModel", recordId: {}, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'recordId' key that is a string, object given")

        describe "it does contain recordId key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: "MyModel", recordId: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'recordId' key that is a string, number given")


      describe "but verbose validations are not passed", ->
        describe "it does contain verbose key but it is a string", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: "MyModel", verbose: "true", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'verbose' key that is a boolean, string given")

        describe "it does contain verbose key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Record({ auth: successfulAuth, appName: "myapp", model: "MyModel", verbose: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataRecord must contain 'verbose' key that is a boolean, number given")
