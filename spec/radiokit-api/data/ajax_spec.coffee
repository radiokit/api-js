"use strict"

RadioKit = require("../../../lib/radiokit-api")


describe "RadioKit.Data.Ajax", ->
  describe "constructor", ->
    describe "if no argument is passed", ->
      it "does throw an error", ->
        expect ->
          new RadioKit.Data.Ajax()
        .toThrow()

    describe "if argument other than object is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Data.Ajax("abcd")
        .toThrow()

    describe "if argument that is an object is passed", ->
      describe "but it does not contain auth key", ->
        it "throws an error", ->
          expect ->
            new RadioKit.Data.Ajax({ })
          .toThrow()

      describe "and it does contain required keys of auth", ->
        describe "but auth is not an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Ajax({ auth: 1234 })
            .toThrow()

        describe "and auth is an object", ->
          auth = null
          beforeEach ->
            auth = new RadioKit.Auth.Editor.OAuth2({ clientId: "1234", redirectUrl: "http://localhost"} )

          it "does not throw an error", ->
            expect ->
              new RadioKit.Data.Ajax({ auth: auth })
            .not.toThrow()

          it "assigns value of auth to options.auth", ->
            instance = new RadioKit.Data.Ajax({ auth: auth })
            expect(instance.options.auth).toEqual auth

          describe "but it does not contain optional verbose key", ->
            it "assigns false to options.verbose", ->
              instance = new RadioKit.Data.Ajax({ auth: auth })
              expect(instance.options.verbose).toEqual false

          describe "and it contains optional verbose key", ->
            describe "but it is not a boolean", ->
              it "throws an error", ->
                expect ->
                  new RadioKit.Data.Ajax({ auth: auth, verbose: 1234 })
                .toThrow()

            describe "and it is a boolean", ->
              it "does not throw an error", ->
                expect ->
                  new RadioKit.Data.Ajax({ auth: auth, verbose: true })
                .not.toThrow()

              it "assigns value of verbose to options.verbose", ->
                instance = new RadioKit.Data.Ajax({ auth: auth, verbose: true })
                expect(instance.options.verbose).toEqual true


  describe "#teardown", ->
    instance = null
    beforeEach ->
      instance = new RadioKit.Data.Ajax({ auth: new RadioKit.Auth.Editor.OAuth2({ clientId: "1234", redirectUrl: "http://localhost" }) })

    it "aborts the request", ->
      spyOn(instance, "abort")

      instance.teardown()

      expect(instance.abort).toHaveBeenCalled()


    xit "calls teardown on parent", ->
      spyOn(instance.__proto__, "teardown")

      instance.teardown()

      expect(instance.__proto__.teardown).toHaveBeenCalled()


  for method in [ "get", "put", "post", "patch", "delete" ]
    describe "##{method}", ->
      instance = null
      beforeEach ->
        auth = new RadioKit.Auth.Editor.OAuth2({ clientId: "1234", redirectUrl: "http://localhost" })

        # TODO write also specs for unauthenticated
        spyOn(auth, "isSignedIn").and.returnValue(true)

        instance = new RadioKit.Data.Ajax({ auth: auth })

        # TODO
        global.XMLHttpRequest = class XMLHttpRequest
          addEventListener: =>
          removeEventListener: =>
          open: =>
          send: =>
          setRequestHeader: =>


      describe "if no argument is passed", ->
        it "does throw an error", ->
          expect ->
            instance[method]()
          .toThrow()

      describe "if argument other than string is passed", ->
        it "throws an error", ->
          expect ->
            instance[method](1234)
          .toThrow()

      describe "if argument that is a string is passed", ->
        it "does not throw an error", ->
          expect ->
            instance[method]("/abc")
          .not.toThrow()
