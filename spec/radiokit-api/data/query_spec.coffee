"use strict"

RadioKit = require("../../../lib/radiokit-api")

describe "RadioKit.Data.Query", ->
  successfulAuth = new RadioKit.Auth.Editor.OAuth2({ clientId: "abc", redirectUrl: "def" })

  describe "constructor", ->
    describe "if no argument is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Data.Query()
        .toThrowError(TypeError, "Missing argument options in RadioKitDataQuery constructor")

    describe "if argument other than object is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Data.Query("abcd")
        .toThrowError(TypeError, "Options passed to RadioKitDataQuery must be an object, string given")

    describe "if object is passed as an argument", ->
      describe "but auth validations are not passed", ->
        describe "it does not contain auth key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ model: "Track", appName: "abc", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'auth' key that is a object, no or undefined value was passed")

        describe "it does contain auth key but it is a string", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ model: "Track", appName: "abc", appConfig: {}, auth: "Abc" })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'auth' key that is a object, string given")

        describe "it does contain auth key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ model: "Track", appName: "abc", appConfig: {}, auth: 123 })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'auth' key that is a object, number given")

      describe "but appName validations are not passed", ->
        describe "it does not contain appName key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, model: "Track", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'appName' key that is a string, no or undefined value was passed")

        describe "it does contain appName key but it is an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, model: "Track", appName: {}, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'appName' key that is a string, object given")

        describe "it does contain appName key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, model: "Track", appName: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'appName' key that is a string, number given")

      describe "but appConfig validations are not passed", ->
        describe "it does not contain appConfig key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ model: "Track", appName: "abc", auth: successfulAuth })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'appConfig' key that is a object, no or undefined value was passed")

        describe "it does contain appConfig key but it is not an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ model: "Track", appName: "abc", auth: successfulAuth, appConfig: "Abc" })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'appConfig' key that is a object, string given")

        describe "it does contain appConfig key but it is not a an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ model: "Track", appName: "abc", auth: successfulAuth, appConfig: 123 })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'appConfig' key that is a object, number given")

      describe "but model validations are not passed", ->
        describe "it does not contain model key", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'model' key that is a string, no or undefined value was passed")

        describe "it does contain model key but it is an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", model: {}, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'model' key that is a string, object given")

        describe "it does contain model key but it is not a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", model: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'model' key that is a string, number given")


      describe "but apiVersion validations are not passed", ->
        describe "it does contain apiVersion key but it is an object", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", model: "MyModel", apiVersion: {}, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'apiVersion' key that is a string, object given")

        describe "it does contain apiVersion key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", model: "MyModel", apiVersion: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'apiVersion' key that is a string, number given")


      describe "but verbose validations are not passed", ->
        describe "it does contain verbose key but it is a string", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", model: "MyModel", verbose: "true", appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'verbose' key that is a boolean, string given")

        describe "it does contain verbose key but it is a number", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", model: "MyModel", verbose: 123, appConfig: {} })
            .toThrowError(TypeError, "Options passed to RadioKitDataQuery must contain 'verbose' key that is a boolean, number given")


  describe "#order", ->
    instance = null
    param = "myparam"

    beforeEach ->
      instance = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" })

    describe "if passed 'asc' as direction", ->
      direction = "asc"

      describe "and there were no earlier order calls", ->
        beforeEach ->
          instance.order(param, direction)

        it "should store passed param and direction", ->
          expect(instance.getOrder()).toEqual([[param, direction]])

      describe "and there were some earlier order calls", ->
        beforeEach ->
          instance.order("oldparam", "desc")
          instance.order(param, direction)

        it "should store passed param and direction", ->
          expect(instance.getOrder()).toEqual([["oldparam", "desc"], [param, direction]])

    describe "if passed 'desc' as direction", ->
      direction = "desc"

      describe "and there were no earlier order calls", ->
        beforeEach ->
          instance.order(param, direction)

        it "should store passed param and direction", ->
          expect(instance.getOrder()).toEqual([[param, direction]])

      describe "and there were some earlier order calls", ->
        beforeEach ->
          instance.order("oldparam", "asc")
          instance.order(param, direction)

        it "should store passed param and direction", ->
          expect(instance.getOrder()).toEqual([["oldparam", "asc"], [param, direction]])

    describe "if passed something else than 'asc' or 'desc' as direction", ->
      it "throws an error", ->
        expect ->
          instance.order(param, "somethingwrong")
        .toThrowError(RangeError, "Invalid direction \"somethingwrong\"")


  describe "#getCollectionParams", ->
    query = null

    beforeEach ->
      query = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" })

    describe "if condition was specified", ->
      describe "and operator is 'in'", ->
        describe "and values are passed as arguments", ->
          beforeEach ->
            query.where("param", "in", "abc", "def")

          it "creates c[param][]=operator in val1 val2 val3 URL syntax", ->
            expect(query.getCollectionParams()).toEqual("c[param][]=in%20abc%20def")

        describe "and values are passed as array", ->
          beforeEach ->
            query.where("param", "in", ["abc", "def"])

          it "creates c[param][]=operator in val1 val2 val3 URL syntax", ->
            expect(query.getCollectionParams()).toEqual("c[param][]=in%20abc%20def")


    describe "if one scope was specified", ->
      describe "and it has no params", ->
        beforeEach ->
          query.scope("somescope")

        it "creates s[]=scopeName", ->
          expect(query.getCollectionParams()).toEqual("s[]=somescope")

      describe "and it has some params", ->
        beforeEach ->
          query.scope("somescope", "a", "b", "c")

        it "creates s[]=scopeName params...", ->
          expect(query.getCollectionParams()).toEqual("s[]=somescope%20a%20b%20c")


    describe "if many scopes was specified", ->
      beforeEach ->
        query.scope("somescope")
        query.scope("otherscope", "a", "b", "c")

      it "adds all of them", ->
        expect(query.getCollectionParams()).toEqual("s[]=somescope&s[]=otherscope%20a%20b%20c")


  describe "#clone", ->
    instance = null
    instanceForThisSpec = null

    beforeEach ->
      instance = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" })

    it "returns an object", ->
      expect(typeof(instance.clone())).toEqual("object")

    describe "which options", ->
      it "are reference to the options from the parent instance", ->
        expect(instance.clone().options).toBe(instance.options)

    describe "which auto update", ->
      instanceForThisSpec = null
      beforeEach ->
        instanceForThisSpec = instance.clone()
        spyOn(instanceForThisSpec, "fetch").and.returnValue(instanceForThisSpec)

      it "is disabled if the parent instance had disabled auto update", ->
        instanceForThisSpec.disableAutoUpdate()
        expect(instanceForThisSpec.clone().isAutoUpdateEnabled()).toBeFalsy()

      xit "is disabled even if the parent instance had enabled auto update", ->
        expect(instanceForThisSpec.clone().isAutoUpdateEnabled()).toBeTruthy()

    describe "which data", ->
      it "are reference to the data from the parent instance", ->
        expect(instance.clone().getData()).toBe(instance.getData())

    describe "which select", ->
      beforeEach ->
        instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).select("a", "b")

      it "is not a reference to select from the parent instance", ->
        expect(instanceForThisSpec.clone().getSelect()).not.toBe(instanceForThisSpec.getSelect())

      it "is equal to select from the parent instance", ->
        expect(instanceForThisSpec.clone().getSelect()).toEqual(instanceForThisSpec.getSelect())

    describe "which where", ->
      beforeEach ->
        instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).where("a", "eq", "b")

      it "is not a reference to where from the parent instance", ->
        expect(instanceForThisSpec.clone().getWhere()).not.toBe(instanceForThisSpec.getWhere())

      it "is equal to where from the parent instance", ->
        expect(instanceForThisSpec.clone().getWhere()).toEqual(instanceForThisSpec.getWhere())

    describe "which joins", ->
      beforeEach ->
        instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).joins("abc")

      it "is not a reference to joins from the parent instance", ->
        expect(instanceForThisSpec.clone().getJoins()).not.toBe(instanceForThisSpec.getJoins())

      it "is equal to joins from the parent instance", ->
        expect(instanceForThisSpec.clone().getJoins()).toEqual(instanceForThisSpec.getJoins())

    describe "which scopes", ->
      beforeEach ->
        instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).scope("abc", "1", "2")

      it "is not a reference to joins from the parent instance", ->
        expect(instanceForThisSpec.clone().getScopes()).not.toBe(instanceForThisSpec.getScopes())

      it "is equal to joins from the parent instance", ->
        expect(instanceForThisSpec.clone().getScopes()).toEqual(instanceForThisSpec.getScopes())

    describe "which limit", ->
      beforeEach ->
        instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).limit(123)

      it "is equal to limit from the parent instance", ->
        expect(instanceForThisSpec.clone().getLimit()).toEqual(instanceForThisSpec.getLimit())

    describe "which offset", ->
      beforeEach ->
        instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).offset(123)

      it "is equal to offset from the parent instance", ->
        expect(instanceForThisSpec.clone().getOffset()).toEqual(instanceForThisSpec.getOffset())

    describe "which order", ->
      beforeEach ->
        instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).order("abc", "asc")

      it "is not a reference to order from the parent instance", ->
        expect(instanceForThisSpec.clone().getOrder()).not.toBe(instanceForThisSpec.getOrder())

      it "is equal to order from the parent instance", ->
        expect(instanceForThisSpec.clone().getOrder()).toEqual(instanceForThisSpec.getOrder())

    describe "which countTotal", ->
      describe "if was disabled in the parent instance", ->
        beforeEach ->
          instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" })

        it "is equal to offset from the parent instance", ->
          expect(instanceForThisSpec.clone().getCountTotal()).toBeFalsy()

      describe "if was enabled in the parent instance", ->
        beforeEach ->
          instanceForThisSpec = new RadioKit.Data.Query({ auth: successfulAuth, appName: "myapp", appConfig: { baseUrl: "http://example.com" }, model: "Track" }).countTotal()

        it "is equal to offset from the parent instance", ->
          expect(instanceForThisSpec.clone().getCountTotal()).toBeTruthy()
