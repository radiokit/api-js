"use strict"

RadioKit = require("../../../lib/radiokit-api")


describe "RadioKit.Data.Interface", ->
  describe "#fromGlobalID", ->
    instance = null

    beforeEach ->
      instance = new RadioKit.Data.Interface({ auth: { clientId: "123" }, apps: { appName: { baseUrl: "http://example.com" } } })

    describe "if valid arguments are passed", ->
      describe "that describe Record's global ID", ->
        it "returns Record object built of passed arguments", ->
          obj = instance.fromGlobalID("record://appName/Model.Name/123")

          expect(obj instanceof RadioKit.Data.Record).toBe(true)
          expect(obj.options.appName).toEqual "appName"
          expect(obj.options.model).toEqual "Model.Name"
          expect(obj.options.recordId).toEqual "123"
          expect(obj.options.auth).toEqual instance.options.auth
          expect(obj.options.appConfig).toEqual instance.options.apps["appName"]
          expect(obj.options.verbose).toEqual instance.options.verbose
          expect(obj.options.apiVersion).toEqual instance.options.apiVersion



#   describe "constructor", ->
#     describe "if no argument is passed", ->
#       it "does throw an error", ->
#         expect ->
#           new RadioKit.Data.Interface()
#         .toThrow()

#     describe "if argument other than object is passed", ->
#       it "throws an error", ->
#         expect ->
#           new RadioKit.Data.Interface("abcd")
#         .toThrow()


#   describe "#addCollection", ->
#     instance = null

#     beforeEach ->
#       instance = new RadioKit.Data.Interface({ authClientId: "abc", authRedirectUrl: "http://localhost" })

#     describe "if no argument is passed", ->
#       it "throws an error", ->
#         expect ->
#           instance.addCollection()
#         .toThrow()

#     describe "if both arguments are passed", ->
#       describe "and first argument", ->
#         describe "is invalid", ->
#           describe "because it contains something different than a string", ->
#             it "throws an error", ->
#               expect ->
#                 instance.addCollection("myCollection", 123)
#               .toThrow()

#         describe "is valid", ->
#           for model in RadioKit.Data.MODELS
#             describe "and it contains valid model name ('#{model}')", ->
#               it "does not throw an error", ->
#                 expect ->
#                   instance.addCollection("myCollection", model)
#                 .not.toThrow()

#       describe "and second argument", ->
#         describe "is invalid", ->
#           describe "because it contains something different than a string", ->
#             it "throws an error", ->
#               expect ->
#                 instance.addCollection(123, "Track")
#               .toThrow()

#         describe "is valid", ->
#           describe "and it contains valid string identyfying query", ->
#             describe "and such identifier is not taken yet", ->
#               it "does not throw an error", ->
#                 expect ->
#                   instance.addCollection("myCollection", "Track")
#                 .not.toThrow()

#               it "returns an object that is instance of RadioKit.Data.Collection", ->
#                 expect(instance.addCollection("myCollection", "Track") instanceof RadioKit.Data.Collection).toBe(true)


#             describe "but such identifier is already taken", ->
#               it "throws an error", ->
#                 instance.addCollection("myCollection", "Track")

#                 expect ->
#                   instance.addCollection("myCollection", "Track")
#                 .toThrow()


#   describe "#removeCollection", ->
#     instance = null

#     beforeEach ->
#       instance = new RadioKit.Data.Interface({ authClientId: "abc", authRedirectUrl: "http://localhost" })

#     describe "if no argument is passed", ->
#       it "throws an error", ->
#         expect ->
#           instance.removeCollection()
#         .toThrow()

#     describe "if one argument is passed", ->
#       describe "but it is invalid", ->
#         describe "because it contains something different than a string", ->
#           it "throws an error", ->
#             expect ->
#               instance.removeCollection(123)
#             .toThrow()

#       describe "and it is valid", ->
#         describe "but there's no such query", ->
#           it "throws an error", ->
#             expect ->
#               instance.removeCollection("myCollection")
#             .toThrow()

#         describe "and there's such query", ->
#           beforeEach ->
#             instance.addCollection("myCollection", "Track")

#           it "does not throw an error", ->
#             expect ->
#               instance.removeCollection("myCollection")
#             .not.toThrow()

#           it "removes the query", ->
#             expect(instance.hasCollection("myCollection")).toBe(true)
#             instance.removeCollection("myCollection")
#             expect(instance.hasCollection("myCollection")).toBe(false)


#   describe "#hasCollection", ->
#     instance = null

#     beforeEach ->
#       instance = new RadioKit.Data.Interface({ authClientId: "abc", authRedirectUrl: "http://localhost" })

#     describe "if no argument is passed", ->
#       it "throws an error", ->
#         expect ->
#           instance.hasCollection()
#         .toThrow()

#     describe "if one argument is passed", ->
#       describe "but it is invalid", ->
#         describe "because it contains something different than a string", ->
#           it "throws an error", ->
#             expect ->
#               instance.hasCollection(123)
#             .toThrow()

#       describe "and it is valid", ->
#         describe "but there's no such query", ->
#           it "returns false", ->
#             expect(instance.hasCollection("myCollection")).toBe(false)

#         describe "and there's such query", ->
#           beforeEach ->
#             instance.addCollection("myCollection", "Track")

#           it "returns true", ->
#             expect(instance.hasCollection("myCollection")).toBe(true)



#   describe "#getCollection", ->
#     instance = null

#     beforeEach ->
#       instance = new RadioKit.Data.Interface({ authClientId: "abc", authRedirectUrl: "http://localhost" })

#     describe "if no argument is passed", ->
#       it "throws an error", ->
#         expect ->
#           instance.getCollection()
#         .toThrow()

#     describe "if one argument is passed", ->
#       describe "but it is invalid", ->
#         describe "because it contains something different than a string", ->
#           it "throws an error", ->
#             expect ->
#               instance.getCollection(123)
#             .toThrow()

#       describe "and it is valid", ->
#         describe "but there's no such query", ->
#           it "throws an error", ->
#             expect ->
#               instance.getCollection("myCollection")
#             .toThrow()

#         describe "and there's such query", ->
#           beforeEach ->
#             instance.addCollection("myCollection", "Track")

#           it "returns an object that is instance of RadioKit.Data.Collection", ->
#             expect(instance.getCollection("myCollection") instanceof RadioKit.Data.Collection).toBe(true)


#   describe "#hasCollection", ->
#     instance = null

#     beforeEach ->
#       instance = new RadioKit.Data.Interface({ authClientId: "abc", authRedirectUrl: "http://localhost" })

#     describe "if no argument is passed", ->
#       it "throws an error", ->
#         expect ->
#           instance.hasCollection()
#         .toThrow()

#     describe "if one argument is passed", ->
#       describe "but it is invalid", ->
#         describe "because it contains something different than a string", ->
#           it "throws an error", ->
#             expect ->
#               instance.hasCollection(123)
#             .toThrow()

#       describe "and it is valid", ->
#         describe "but there's no such query", ->
#           it "returns false", ->
#             expect(instance.hasCollection("myCollection")).toBe(false)

#         describe "and there's such query", ->
#           beforeEach ->
#             instance.addCollection("myCollection", "Track")

#           it "returns true", ->
#             expect(instance.hasCollection("myCollection")).toBe(true)


#   describe "#getSnapshot", ->
#     instance = null

#     beforeEach ->
#       instance = new RadioKit.Data.Interface({ authClientId: "abc", authRedirectUrl: "http://localhost" })

#     describe "after object construction", ->
#       it "is an empty object", ->
#         expect(instance.getSnapshot()).toEqual({})

#     describe "after adding a query", ->
#       beforeEach ->
#         instance.addCollection("myCollection", "Track")

#       it "contains a key that is equal to query ID, with empty array as a value", ->
#         expect(instance.getSnapshot()).toEqual({ myCollection: [] })

#       xit "triggers 'snapshot' event"

#     describe "after removing a query", ->
#       beforeEach ->
#         instance.addCollection("myCollection1", "Track")
#         instance.addCollection("myCollection2", "Track")
#         instance.removeCollection("myCollection1")

#       it "removes keys that are equal to removed query ID", ->
#         expect(instance.getSnapshot()).toEqual({ myCollection2: [] })

#       xit "triggers 'snapshot' event"


#   describe "#teardown", ->
#     instance = null

#     beforeEach ->
#       instance = new RadioKit.Data.Interface({ authClientId: "abc", authRedirectUrl: "http://localhost" })

#     describe "if there are some queries", ->
#       beforeEach ->
#         instance.addCollection("myCollection", "Track")

#       xit "removes them"
#       xit "calls teardown of base class"
