"use strict"

RadioKit = require("../../../../lib/radiokit-api")


describe "RadioKit.Auth.Editor.OAuth2", ->
  describe "constructor", ->
    describe "if no argument is passed", ->
      it "does throw an error", ->
        expect ->
          new RadioKit.Auth.Editor.OAuth2()
        .toThrow()

    describe "if argument other than object is passed", ->
      it "throws an error", ->
        expect ->
          new RadioKit.Auth.Editor.OAuth2("abcd")
        .toThrow()

    describe "if argument that is an object is passed", ->
      describe "but it does not contain clientId key", ->
        it "throws an error", ->
          expect ->
            new RadioKit.Auth.Editor.OAuth2({ redirectUrl: "http://localhost" })
          .toThrow()

      describe "and it does contain required keys of clientId", ->
        describe "but clientId is not a string", ->
          it "throws an error", ->
            expect ->
              new RadioKit.Auth.Editor.OAuth2({ clientId: 1234 })
            .toThrow()


        describe "and clientId is a string", ->
          it "does not throw an error", ->
            expect ->
              new RadioKit.Auth.Editor.OAuth2({ clientId: "1234" })
            .not.toThrow()

          it "assigns value of clientId to options.clientId", ->
            instance = new RadioKit.Auth.Editor.OAuth2({ clientId: "1234" })
            expect(instance.options.clientId).toEqual "1234"

        describe "but it does not contain optional baseUrl key", ->
          it "assigns 'https://auth.radiokitapp.org' to options.baseUrl", ->
            instance = new RadioKit.Auth.Editor.OAuth2({ clientId: "1234" })
            expect(instance.options.baseUrl).toEqual "https://auth.radiokitapp.org"

        describe "and it contains optional baseUrl key", ->
          describe "but it is not a string", ->
            it "throws an error", ->
              expect ->
                new RadioKit.Auth.Editor.OAuth2({ clientId: "1234", baseUrl: 1234 })
              .toThrow()

          describe "and it is a string", ->
            it "does not throw an error", ->
              expect ->
                new RadioKit.Auth.Editor.OAuth2({ clientId: "1234", baseUrl: "http://engine.test" })
              .not.toThrow()

            it "assigns value of baseUrl to options.baseUrl", ->
              instance = new RadioKit.Auth.Editor.OAuth2({ clientId: "1234", baseUrl: "http://engine.test" })
              expect(instance.options.baseUrl).toEqual "http://engine.test"


  describe "#signIn", ->
    clientId      = "1234/" # Intentionally includes special character to test urlencoding
    redirectUrl   = "http://localhost" # Intentionally includes special character to test urlencoding
    baseUrl = "http://engine.test"
    instance      = null

    beforeEach ->
      instance = new RadioKit.Auth.Editor.OAuth2({ clientId: clientId, baseUrl: baseUrl })

    afterEach ->
      instance.teardown() # Cleanup event handlers

    describe "if already authenticated", ->
      beforeEach ->
        spyOn(instance, "isSignedIn").and.returnValue(true)

      it "throws an error", ->
        expect ->
          instance.signIn()
        .toThrow()

    describe "if window object is available", ->
      describe "and there are some hash params", ->
        describe "and they contain error key", ->
          beforeEach ->
            global.window = { location: { hash: "#error=somereason" } }

          afterEach ->
            delete global.window

          it "should trigger 'failure' event with reason as the argument", ->
            spyOn(instance, "trigger")

            instance.signIn()

            expect(instance.trigger).toHaveBeenCalledWith("failure", "somereason")

        describe "and they contain access_token and token_type key", ->
          beforeEach ->
            global.window = { location: { hash: "#access_token=123456&token_type=bearer" } }

          afterEach ->
            delete global.window

          xit "should trigger 'success' event", ->
            spyOn(instance, "trigger")
            # TODO fails on missing sessionStorage
            instance.signIn()

            expect(instance.trigger).toHaveBeenCalledWith("success")

        describe "and they do not contain any of error and access_token/token_type pair", ->
          beforeEach ->
            global.window = { location: { hash: "#param1=val1", origin: "http://thishost.com", pathname: "/abcd", search: "?abcd=def", assign: -> } }

          afterEach ->
            delete global.window

          xit "should redirect to (ENGINE_BASE_URL)/api/oauth2/authorize?response_type=token&client_id=(URLENCODED_CLIENT_ID)&redirect_uri=(URLENCODED_ORIGIN)&state=(RANDOM_STATE)|(REDIRECT_PATH)", ->
            spyOn(global.window.location, "assign")

            instance.signIn()

            # TODO check for state

            expect(global.window.location.assign).toHaveBeenCalledWith(instance.options.baseUrl + "/api/oauth2/authorize?response_type=token&client_id=" + encodeURIComponent(instance.options.clientId) + "&redirect_uri=" + encodeURIComponent("http://thishost.com"))

      describe "and there are no hash params", ->
        beforeEach ->
          global.window = { location: { hash: "", origin: "http://thishost.com", pathname: "/abcd", search: "?abcd=def", assign: -> } }

        afterEach ->
          delete global.window

        xit "should redirect to (ENGINE_BASE_URL)/api/oauth2/authorize?response_type=token&client_id=(URLENCODED_CLIENT_ID)&redirect_uri=(URLENCODED_ORIGIN)&state=(RANDOM_STATE)|(REDIRECT_PATH)", ->
          spyOn(global.window.location, "assign")

          instance.signIn()

          # TODO check for state

          expect(global.window.location.assign).toHaveBeenCalledWith(instance.options.baseUrl + "/api/oauth2/authorize?response_type=token&client_id=" + encodeURIComponent(instance.options.clientId) + "&redirect_uri=" + encodeURIComponent("http://thishost.com"))


  describe "#signOut", ->
    clientId      = "1234/" # Intentionally includes special character to test urlencoding
    redirectUrl   = "http://localhost" # Intentionally includes special character to test urlencoding
    baseUrl = "http://engine.test"
    instance      = null

    beforeEach ->
      instance = new RadioKit.Auth.Editor.OAuth2({ clientId: clientId, baseUrl: baseUrl })

    afterEach ->
      instance.teardown() # Cleanup event handlers

    describe "if not authenticated", ->
      beforeEach ->
        spyOn(instance, "isSignedIn").and.returnValue(false)

      it "throws an error", ->
        expect ->
          instance.signOut()
        .toThrow()

      # TODO
