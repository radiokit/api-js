"use strict"

RadioKit = require("../../lib/radiokit-api")


describe "RadioKit.Data", ->
  describe ".buildRecordGlobalID", ->
    describe "if valid arguments are passed", ->
      it "returns string of record://app/model/id", ->
        expect(RadioKit.Data.buildRecordGlobalID("appName", "Model.Name", "123")).toEqual "record://appName/Model.Name/123"
