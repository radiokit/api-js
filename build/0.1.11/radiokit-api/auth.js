(function() {
  "use strict";
  var AuthEditor;

  AuthEditor = require('./auth/editor');

  module.exports = {
    "Editor": AuthEditor
  };

}).call(this);
