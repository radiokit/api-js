(function() {
  "use strict";
  var RadioKitAuthEditorBase, RadioKitBase,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RadioKitBase = require('../../base');

  RadioKitAuthEditorBase = (function(superClass) {
    extend(RadioKitAuthEditorBase, superClass);

    RadioKitAuthEditorBase.prototype.EVENTS = ["success", "failure", "signout"];

    function RadioKitAuthEditorBase() {
      this._deauthenticate = bind(this._deauthenticate, this);
      this._authenticate = bind(this._authenticate, this);
      this.signOut = bind(this.signOut, this);
      this.signIn = bind(this.signIn, this);
      this.isSignedIn = bind(this.isSignedIn, this);
      this.getPassword = bind(this.getPassword, this);
      this.getUsername = bind(this.getUsername, this);
      this.getHeaders = bind(this.getHeaders, this);
      this.__signedIn = false;
      this.__headers = {};
      this.__username = null;
      this.__password = null;
    }

    RadioKitAuthEditorBase.prototype.getHeaders = function() {
      if (this.isSignedIn() === false) {
        throw new Error("Not authenticated");
      }
      return this.__headers;
    };

    RadioKitAuthEditorBase.prototype.getUsername = function() {
      if (this.isSignedIn() === false) {
        throw new Error("Not authenticated");
      }
      return this.__username;
    };

    RadioKitAuthEditorBase.prototype.getPassword = function() {
      if (this.isSignedIn() === false) {
        throw new Error("Not authenticated");
      }
      return this.__username;
    };

    RadioKitAuthEditorBase.prototype.isSignedIn = function() {
      return this.__signedIn;
    };

    RadioKitAuthEditorBase.prototype.signIn = function() {};

    RadioKitAuthEditorBase.prototype.signOut = function() {};

    RadioKitAuthEditorBase.prototype._authenticate = function(headers, username, password) {
      if (this.isSignedIn() === true) {
        throw new Error("Already authenticated");
      }
      if (typeof headers !== "object") {
        throw new TypeError("Headers must be an object");
      }
      if (typeof username !== "undefined") {
        if (typeof username !== "string") {
          throw new TypeError("Username (if present) must be a string");
        }
      }
      if (typeof password !== "undefined") {
        if (typeof password !== "string") {
          throw new TypeError("Password (if present) must be a string");
        }
      }
      this.__signedIn = true;
      this.__headers = headers;
      this.__username = username;
      return this.__password = password;
    };

    RadioKitAuthEditorBase.prototype._deauthenticate = function() {
      if (this.isSignedIn() === false) {
        throw new Error("Not authenticated");
      }
      this.__signedIn = false;
      this.__headers = {};
      this.__username = null;
      return this.__password = null;
    };

    return RadioKitAuthEditorBase;

  })(RadioKitBase);

  module.exports = RadioKitAuthEditorBase;

}).call(this);
