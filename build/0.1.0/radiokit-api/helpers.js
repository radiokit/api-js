(function() {
  "use strict";
  var HelpersLocation, HelpersString;

  HelpersString = require('./helpers/string');

  HelpersLocation = require('./helpers/location');

  module.exports = {
    "String": HelpersString,
    "Location": HelpersLocation
  };

}).call(this);
