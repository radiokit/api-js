(function() {
  "use strict";
  var RadioKitAuthEditorBase, RadioKitAuthEditorOAuth2, RadioKitHelpersLocation,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RadioKitHelpersLocation = require('../../helpers/location');

  RadioKitAuthEditorBase = require('./base');

  RadioKitAuthEditorOAuth2 = (function(superClass) {
    extend(RadioKitAuthEditorOAuth2, superClass);

    function RadioKitAuthEditorOAuth2(options) {
      this.options = options;
      this.signOut = bind(this.signOut, this);
      this.signIn = bind(this.signIn, this);
      RadioKitAuthEditorOAuth2.__super__.constructor.apply(this, arguments);
      this._validateConstructorOptions();
      this._validateConstructorOption("accessToken", "string", false);
      if (this.options.accessToken != null) {
        this._authenticate({
          "Authorization": "Bearer " + this.options.accessToken
        });
      } else {
        this._validateConstructorOption("clientId", "string", true);
        this._validateConstructorOption("baseUrl", "string", false, "https://auth.radiokitapp.org");
      }
    }

    RadioKitAuthEditorOAuth2.prototype.signIn = function() {
      var hashParams;
      if (typeof window !== "undefined" && window !== null) {
        hashParams = RadioKitHelpersLocation.parseHash(window.location.hash);
        if (hashParams.hasOwnProperty("error")) {
          this.trigger("failure", hashParams.error);
          return;
        }
        if (hashParams.hasOwnProperty("access_token")) {
          this._authenticate({
            "Authorization": "Bearer " + hashParams.access_token
          });
          this.trigger("success");
          return;
        }
        return window.location.assign(this.options.baseUrl + "/api/oauth2/authorize?response_type=token&client_id=" + encodeURIComponent(this.options.clientId) + "&redirect_uri=" + encodeURIComponent(window.location.origin + window.location.pathname + window.location.search));
      } else {
        throw new Error("TODO");
      }
    };

    RadioKitAuthEditorOAuth2.prototype.signOut = function() {
      this._deauthenticate();
      return this.trigger("signout");
    };

    return RadioKitAuthEditorOAuth2;

  })(RadioKitAuthEditorBase);

  module.exports = RadioKitAuthEditorOAuth2;

}).call(this);
