(function() {
  "use strict";
  var AuthEditorOAuth2;

  AuthEditorOAuth2 = require('./editor/oauth2');

  module.exports = {
    "OAuth2": AuthEditorOAuth2
  };

}).call(this);
