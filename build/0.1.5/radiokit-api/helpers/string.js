(function() {
  "use strict";
  var RadioKitHelpersString;

  RadioKitHelpersString = (function() {
    function RadioKitHelpersString() {}

    RadioKitHelpersString.underscore = function(string) {
      var underscored;
      if (typeof string !== "string") {
        throw new TypeError("Argument passed to underscore must be a string");
      }
      if (string[0].toLowerCase() === string[0]) {
        underscored = string.replace(/([A-Z])/g, function(_, c) {
          return "_" + (c.toLowerCase());
        });
      } else {
        underscored = string.replace(/([A-Z])/g, function(_, c) {
          return "_" + (c.toLowerCase());
        }).substr(1);
      }
      return underscored.replace(/(::_)/g, "::").replace(/(::)/g, "/");
    };

    RadioKitHelpersString.modelNameToUrl = function(string) {
      if (typeof string !== "string") {
        throw new TypeError("Argument passed to underscore must be a string");
      }
      return string.toLowerCase().replace(/\./g, "/");
    };

    return RadioKitHelpersString;

  })();

  module.exports = RadioKitHelpersString;

}).call(this);
