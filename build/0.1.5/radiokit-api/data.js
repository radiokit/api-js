(function() {
  "use strict";
  var DataAjax, DataInterface, DataQuery, DataRecord, DataUpload;

  DataAjax = require('./data/ajax');

  DataRecord = require('./data/record');

  DataQuery = require('./data/query');

  DataInterface = require('./data/interface');

  DataUpload = require('./data/upload');

  module.exports = {
    "Ajax": DataAjax,
    "Record": DataRecord,
    "Query": DataQuery,
    "Interface": DataInterface,
    "Upload": DataUpload
  };

}).call(this);
