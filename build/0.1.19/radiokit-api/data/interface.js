(function() {
  "use strict";
  var Immutable, ParseURI, RadioKitAuthEditorOAuth2, RadioKitBase, RadioKitDataAjax, RadioKitDataInterface, RadioKitDataQuery, RadioKitDataRecord, RadioKitDataUpload,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RadioKitBase = require('../base');

  RadioKitDataQuery = require('./query');

  RadioKitDataRecord = require('./record');

  RadioKitDataAjax = require('./ajax');

  RadioKitDataUpload = require('./upload');

  RadioKitAuthEditorOAuth2 = require('../auth/editor/oauth2');

  Immutable = require('immutable');

  ParseURI = require('parse-uri');

  RadioKitDataInterface = (function(superClass) {
    extend(RadioKitDataInterface, superClass);

    RadioKitDataInterface.prototype.EVENTS = ["auth::success", "auth::failure"];

    function RadioKitDataInterface(options) {
      this.options = options;
      this.__onRecordTeardown = bind(this.__onRecordTeardown, this);
      this.__onQueryTeardown = bind(this.__onQueryTeardown, this);
      this.__onUploadTeardown = bind(this.__onUploadTeardown, this);
      this.__onAuthFailure = bind(this.__onAuthFailure, this);
      this.__onAuthSuccess = bind(this.__onAuthSuccess, this);
      this.getUploads = bind(this.getUploads, this);
      this.getRecords = bind(this.getRecords, this);
      this.getQueries = bind(this.getQueries, this);
      this.upload = bind(this.upload, this);
      this.record = bind(this.record, this);
      this.query = bind(this.query, this);
      this.fromGlobalID = bind(this.fromGlobalID, this);
      this.signOut = bind(this.signOut, this);
      this.signIn = bind(this.signIn, this);
      this.teardown = bind(this.teardown, this);
      this._validateConstructorOptions();
      this._validateConstructorOption("verbose", "boolean", false, false);
      this._validateConstructorOption("auth", "object", true);
      this._validateConstructorOption("auth.accessToken", "string", false);
      if (this.options.auth.accessToken == null) {
        this._validateConstructorOption("auth.clientId", "string", true);
      }
      this._validateConstructorOption("auth.baseUrl", "string", false);
      this._validateConstructorOption("apps", "object", true);
      this._validateConstructorOption("apiVersion", "string", false, "1.0");
      this.__uploads = new Immutable.Set();
      this.__queries = new Immutable.Set();
      this.__records = new Immutable.Set();
      this.options.auth = new RadioKitAuthEditorOAuth2(this.options.auth);
      this.options.auth.on("success", this.__onAuthSuccess);
      this.options.auth.on("failure", this.__onAuthFailure);
    }

    RadioKitDataInterface.prototype.teardown = function() {
      var i, j, k, len, len1, len2, query, record, ref, ref1, ref2, upload;
      RadioKitDataInterface.__super__.teardown.apply(this, arguments);
      ref = this.__queries;
      for (i = 0, len = ref.length; i < len; i++) {
        query = ref[i];
        query.teardown();
      }
      ref1 = this.__records;
      for (j = 0, len1 = ref1.length; j < len1; j++) {
        record = ref1[j];
        record.teardown();
      }
      ref2 = this.__uploads;
      for (k = 0, len2 = ref2.length; k < len2; k++) {
        upload = ref2[k];
        upload.teardown();
      }
      return this;
    };

    RadioKitDataInterface.prototype.signIn = function() {
      return this.options.auth.signIn();
    };

    RadioKitDataInterface.prototype.signOut = function() {
      return this.options.auth.signOut();
    };

    RadioKitDataInterface.prototype.fromGlobalID = function(globalID) {
      var recordIdParts, uri;
      uri = ParseURI(globalID);
      if (uri.protocol === "record") {
        recordIdParts = uri.path.split("/", 3);
        return this.record(uri.host, recordIdParts[1], recordIdParts[2]);
      } else {
        throw new RangeError("Unknown type of global ID");
      }
    };

    RadioKitDataInterface.prototype.query = function(appName, model) {
      var query;
      if (!this.options.apps.hasOwnProperty(appName)) {
        throw new RangeError("Application '" + appName + "' is unknown");
      }
      query = new RadioKitDataQuery({
        "auth": this.options.auth,
        "appName": appName,
        "appConfig": this.options.apps[appName],
        "model": model,
        "verbose": this.options.verbose,
        "apiVersion": this.options.apiVersion
      });
      query.on("teardown", this.__onQueryTeardown);
      this.__queries.add(query);
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Interface] Added query for " + appName + " / " + model);
      }
      return query;
    };

    RadioKitDataInterface.prototype.record = function(appName, model, recordId) {
      var record;
      if (!this.options.apps.hasOwnProperty(appName)) {
        throw new RangeError("Application '" + appName + "' is unknown");
      }
      record = new RadioKitDataRecord({
        "auth": this.options.auth,
        "appName": appName,
        "appConfig": this.options.apps[appName],
        "model": model,
        "recordId": recordId,
        "verbose": this.options.verbose,
        "apiVersion": this.options.apiVersion
      });
      record.on("teardown", this.__onRecordTeardown);
      this.__records.add(record);
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Interface] Added record for " + appName + " / " + model + " / " + recordId);
      }
      return record;
    };

    RadioKitDataInterface.prototype.upload = function(recordRepositoryId, uploadOptions) {
      var upload;
      if (!this.options.apps.hasOwnProperty("vault")) {
        throw new RangeError("Application 'vault' is unknown, you must define configuration for 'vault' app if you want to use uploader");
      }
      if (typeof recordRepositoryId !== "string") {
        throw new TypeError("Record Repository ID must be a string, " + (typeof recordRepositoryId) + " given");
      }
      upload = new RadioKitDataUpload({
        "auth": this.options.auth,
        "vaultConfig": this.options.apps.vault,
        "verbose": this.options.verbose,
        "apiVersion": this.options.apiVersion,
        "recordRepositoryId": recordRepositoryId,
        "maxFiles": uploadOptions.maxFiles,
        "autoStart": uploadOptions.autoStart
      });
      upload.on("teardown", this.__onUploadTeardown);
      this.__uploads.add(upload);
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Interface] Added upload for " + recordRepositoryId);
      }
      return upload;
    };

    RadioKitDataInterface.prototype.getQueries = function() {
      return this.__queries;
    };

    RadioKitDataInterface.prototype.getRecords = function() {
      return this.__records;
    };

    RadioKitDataInterface.prototype.getUploads = function() {
      return this.__uploads;
    };

    RadioKitDataInterface.prototype.__onAuthSuccess = function(event, redirect) {
      return this.trigger("auth::success", redirect);
    };

    RadioKitDataInterface.prototype.__onAuthFailure = function(event, reason) {
      return this.trigger("auth::failure", reason);
    };

    RadioKitDataInterface.prototype.__onUploadTeardown = function(event, upload) {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Interface] Removed upload for " + upload.options.recordRepositoryId);
      }
      return this.__uploads["delete"](upload);
    };

    RadioKitDataInterface.prototype.__onQueryTeardown = function(event, query) {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Interface] Removed query for " + query.options.appName + " / " + query.options.model);
      }
      return this.__queries["delete"](query);
    };

    RadioKitDataInterface.prototype.__onRecordTeardown = function(event, record) {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Interface] Removed record for " + record.options.appName + " / " + record.options.model + " / " + record.options.recordId);
      }
      return this.__records["delete"](record);
    };

    return RadioKitDataInterface;

  })(RadioKitBase);

  module.exports = RadioKitDataInterface;

}).call(this);
