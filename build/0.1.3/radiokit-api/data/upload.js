(function() {
  "use strict";
  var Immutable, RadioKitBase, RadioKitDataUpload, Resumable,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RadioKitBase = require('../base');

  Resumable = require('resumablejs');

  Immutable = require('immutable');

  RadioKitDataUpload = (function(superClass) {
    extend(RadioKitDataUpload, superClass);

    RadioKitDataUpload.prototype.EVENTS = ["added", "progress", "retry", "error", "started", "cancelled", "completed"];

    function RadioKitDataUpload(options) {
      this.options = options;
      this.__generateUniqueIdentifier = bind(this.__generateUniqueIdentifier, this);
      this.__onResumableUploadCancelled = bind(this.__onResumableUploadCancelled, this);
      this.__onResumableUploadStarted = bind(this.__onResumableUploadStarted, this);
      this.__onResumableCompleted = bind(this.__onResumableCompleted, this);
      this.__onResumableFileProgress = bind(this.__onResumableFileProgress, this);
      this.__onResumableFileError = bind(this.__onResumableFileError, this);
      this.__onResumableFileRetry = bind(this.__onResumableFileRetry, this);
      this.__onResumableFileAdded = bind(this.__onResumableFileAdded, this);
      this.getQueue = bind(this.getQueue, this);
      this.cancel = bind(this.cancel, this);
      this.start = bind(this.start, this);
      this.assignDrop = bind(this.assignDrop, this);
      this.assignBrowse = bind(this.assignBrowse, this);
      this.teardown = bind(this.teardown, this);
      this._validateConstructorOptions();
      this._validateConstructorOption("verbose", "boolean", false, false);
      this._validateConstructorOption("apiVersion", "string", false, "1.0");
      this._validateConstructorOption("vaultConfig", "object", true);
      this._validateConstructorOption("auth", "object", true);
      this._validateConstructorOption("recordRepositoryId", "string", true);
      this._validateConstructorOption("maxFiles", "number", false);
      this._validateConstructorOption("autoStart", "boolean", false, false);
      this.__resumable = new Resumable({
        testChunks: false,
        forceChunkSize: true,
        chunkSize: 524288,
        target: this.options.vaultConfig.baseUrl + "/api/upload/v" + this.options.apiVersion + "/resumablejs",
        headers: this.options.auth.getHeaders(),
        query: {
          record_repository_id: this.options.recordRepositoryId
        },
        simultaneousUploads: 1,
        minFileSize: 1,
        maxFiles: this.options.maxFiles,
        generateUniqueIdentifier: this.__generateUniqueIdentifier
      });
      if (!this.__resumable.support) {
        throw new Error("Chunked uploading is not supported on this browser");
      }
      this.__resumable.on("fileAdded", this.__onResumableFileAdded);
      this.__resumable.on("fileProgress", this.__onResumableFileProgress);
      this.__resumable.on("fileRetry", this.__onResumableFileRetry);
      this.__resumable.on("fileError", this.__onResumableFileError);
      this.__resumable.on("complete", this.__onResumableCompleted);
      this.__resumable.on("uploadStart", this.__onResumableUploadStarted);
      this.__resumable.on("beforeCancel", this.__onResumableUploadCancelled);
    }

    RadioKitDataUpload.prototype.teardown = function() {
      RadioKitDataUpload.__super__.teardown.apply(this, arguments);
      this.__resumable.off("fileAdded", this.__onResumableFileAdded);
      this.__resumable.off("fileProgress", this.__onResumableFileProgress);
      this.__resumable.off("fileRetry", this.__onResumableFileRetry);
      this.__resumable.off("fileError", this.__onResumableFileError);
      this.__resumable.off("complete", this.__onResumableCompleted);
      this.__resumable.off("uploadStart", this.__onResumableUploadStarted);
      this.__resumable.off("beforeCancel", this.__onResumableUploadCancelled);
      this.__resumable.cancel();
      delete this.__resumable;
      return this;
    };

    RadioKitDataUpload.prototype.assignBrowse = function(domNode) {
      this.__resumable.assignBrowse(domNode);
      return this;
    };

    RadioKitDataUpload.prototype.assignDrop = function(domNode) {
      this.__resumable.assignDrop(domNode);
      return this;
    };

    RadioKitDataUpload.prototype.start = function() {
      return this.__resumable.upload();
    };

    RadioKitDataUpload.prototype.cancel = function() {
      return this.__resumable.cancel();
    };

    RadioKitDataUpload.prototype.getQueue = function() {
      var files;
      files = this.__resumable.files.map((function(_this) {
        return function(file) {
          return {
            id: file.uniqueIdentifier,
            name: file.fileName,
            size: file.size,
            progress: parseInt(file.progress() * 100),
            uploading: file.isUploading(),
            completed: file.isComplete()
          };
        };
      })(this));
      return Immutable.fromJS(files);
    };

    RadioKitDataUpload.prototype.__onResumableFileAdded = function() {
      this.trigger("added", this);
      if (this.options.autoStart) {
        return this.start();
      }
    };

    RadioKitDataUpload.prototype.__onResumableFileRetry = function() {
      return this.trigger("retry", this);
    };

    RadioKitDataUpload.prototype.__onResumableFileError = function() {
      return this.trigger("error", this);
    };

    RadioKitDataUpload.prototype.__onResumableFileProgress = function() {
      return this.trigger("progress", this);
    };

    RadioKitDataUpload.prototype.__onResumableCompleted = function() {
      return this.trigger("completed", this);
    };

    RadioKitDataUpload.prototype.__onResumableUploadStarted = function() {
      return this.trigger("started", this);
    };

    RadioKitDataUpload.prototype.__onResumableUploadCancelled = function() {
      return this.trigger("cancelled", this);
    };

    RadioKitDataUpload.prototype.__generateUniqueIdentifier = function() {
      return this.options.recordRepositoryId + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + Date.now();
    };

    return RadioKitDataUpload;

  })(RadioKitBase);

  module.exports = RadioKitDataUpload;

}).call(this);
