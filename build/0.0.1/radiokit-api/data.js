(function() {
  "use strict";
  var DataAjax, DataInterface, DataQuery, DataRecord;

  DataAjax = require('./data/ajax');

  DataRecord = require('./data/record');

  DataQuery = require('./data/query');

  DataInterface = require('./data/interface');

  module.exports = {
    "Ajax": DataAjax,
    "Record": DataRecord,
    "Query": DataQuery,
    "Interface": DataInterface
  };

}).call(this);
