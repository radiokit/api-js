(function() {
  "use strict";
  var Immutable, RadioKitBase, RadioKitDataAjax, RadioKitDataRecord, RadioKitHelpersString,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RadioKitBase = require('../base');

  RadioKitHelpersString = require('../helpers/string');

  RadioKitDataAjax = require('./ajax');

  Immutable = require('immutable');

  RadioKitDataRecord = (function(superClass) {
    extend(RadioKitDataRecord, superClass);

    RadioKitDataRecord.prototype.EVENTS = ["loading", "loaded", "abort", "warning", "error"];

    function RadioKitDataRecord(options1) {
      this.options = options1;
      this.__onAjaxAbort = bind(this.__onAjaxAbort, this);
      this.__onAjaxError = bind(this.__onAjaxError, this);
      this.__onAjaxLoaded = bind(this.__onAjaxLoaded, this);
      this.__onAjaxLoading = bind(this.__onAjaxLoading, this);
      this.getRecords = bind(this.getRecords, this);
      this.destroy = bind(this.destroy, this);
      this.update = bind(this.update, this);
      this.create = bind(this.create, this);
      this.show = bind(this.show, this);
      this.getRecordLocation = bind(this.getRecordLocation, this);
      this.teardown = bind(this.teardown, this);
      this._validateConstructorOptions();
      this._validateConstructorOption("recordId", "string", false);
      this._validateConstructorOption("verbose", "boolean", false, false);
      this._validateConstructorOption("apiVersion", "string", false, "1.0");
      this._validateConstructorOption("appName", "string", true);
      this._validateConstructorOption("appConfig", "object", true);
      this._validateConstructorOption("auth", "object", true);
      this._validateConstructorOption("model", "string", true);
      this.__ajax = new RadioKitDataAjax({
        "auth": this.options.auth,
        "verbose": this.options.verbose
      });
      this.__ajax.on("loading", this.__onAjaxLoading);
      this.__ajax.on("loaded", this.__onAjaxLoaded);
      this.__ajax.on("error", this.__onAjaxError);
      this.__ajax.on("abort", this.__onAjaxAbort);
    }

    RadioKitDataRecord.prototype.teardown = function() {
      RadioKitDataRecord.__super__.teardown.apply(this, arguments);
      this.__ajax.teardown();
      return this;
    };

    RadioKitDataRecord.prototype.getRecordLocation = function() {
      if (this.options.recordId) {
        return this.options.appConfig.baseUrl + ("/api/rest/v" + this.options.apiVersion + "/" + (RadioKitHelpersString.modelNameToUrl(this.options.model)) + "/" + this.options.recordId);
      } else {
        return this.options.appConfig.baseUrl + ("/api/rest/v" + this.options.apiVersion + "/" + (RadioKitHelpersString.modelNameToUrl(this.options.model)));
      }
    };

    RadioKitDataRecord.prototype.show = function() {
      if (!this.options.recordId) {
        throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key if you want to use show()");
      }
      if (typeof this.options.recordId !== "string") {
        throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key that is a string if you want to use show(), " + (typeof options.recordId) + " given");
      }
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Record] Showing model " + this.options.model + "#" + this.options.recordId);
      }
      this.__ajax.get(this.getRecordLocation());
      return this;
    };

    RadioKitDataRecord.prototype.create = function(attributes) {
      if (this.options.recordId) {
        throw new RangeError("Options passed to RadioKit.Data.Record must not contain recordId key if you want to use create()");
      }
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Record] Creating model " + this.options.model + " with attributes " + (JSON.stringify(attributes)));
      }
      this.__ajax.post(this.getRecordLocation(), attributes);
      return this;
    };

    RadioKitDataRecord.prototype.update = function(attributes) {
      if (!this.options.recordId) {
        throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key if you want to use update()");
      }
      if (typeof this.options.recordId !== "string") {
        throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key that is a string if you want to use update(), " + (typeof options.recordId) + " given");
      }
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Record] Updating model " + this.options.model + "#" + this.options.recordId + " with attributes " + (JSON.stringify(attributes)));
      }
      this.__ajax.patch(this.getRecordLocation(), attributes);
      return this;
    };

    RadioKitDataRecord.prototype.destroy = function() {
      if (!this.options.recordId) {
        throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key if you want to use destroy()");
      }
      if (typeof this.options.recordId !== "string") {
        throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key that is a string if you want to use destroy(), " + (typeof options.recordId) + " given");
      }
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Record] Destroying model " + this.options.model + "#" + this.options.recordId);
      }
      this.__ajax["delete"](this.getRecordLocation());
      return this;
    };

    RadioKitDataRecord.prototype.getRecords = function() {
      if (this.__records === null) {
        throw new Error("Please fetch the data first");
      }
      return this.__records;
    };

    RadioKitDataRecord.prototype.__onAjaxLoading = function(eventName) {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Record] Starting AJAX request for model " + this.options.model + "#" + this.options.recordId);
      }
      return this.trigger("loading", this);
    };

    RadioKitDataRecord.prototype.__onAjaxLoaded = function(eventName, status, response) {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Record] Finished AJAX request for model " + this.options.model + "#" + this.options.recordId + " with status " + status + ", response " + (JSON.stringify(response)));
      }
      if (status >= 200 && status <= 299) {
        return this.trigger("loaded", this, Immutable.fromJS(response.data));
      } else if (status === 422) {
        return this.trigger("warning", this);
      } else {
        return this.trigger("error", this);
      }
    };

    RadioKitDataRecord.prototype.__onAjaxError = function() {
      console.warn("[RadioKit.Data.Record] Error during AJAX request for model " + this.options.model + "#" + this.options.recordId);
      return this.trigger("error", this);
    };

    RadioKitDataRecord.prototype.__onAjaxAbort = function() {
      console.warn("[RadioKit.Data.Record] Aborted AJAX request for model " + this.options.model + "#" + this.options.recordId);
      return this.trigger("abort", this);
    };

    return RadioKitDataRecord;

  })(RadioKitBase);

  module.exports = RadioKitDataRecord;

}).call(this);
