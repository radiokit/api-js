(function() {
  "use strict";
  var RadioKitBase,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  RadioKitBase = (function() {
    function RadioKitBase() {
      this._validateConstructorOption = bind(this._validateConstructorOption, this);
      this._validateConstructorOptions = bind(this._validateConstructorOptions, this);
      this.teardown = bind(this.teardown, this);
      this.trigger = bind(this.trigger, this);
      this.off = bind(this.off, this);
      this.on = bind(this.on, this);
    }

    RadioKitBase.prototype.on = function(event, callback) {
      if (typeof event !== "string") {
        throw new TypeError("Unable to add callback: event name must be a string");
      }
      if (typeof callback !== "function") {
        throw new TypeError("Unable to add callback: Callback must be a function");
      }
      if (indexOf.call(this.EVENTS, event) >= 0) {
        if (typeof this.__callbacks !== "object") {
          this.__callbacks = {};
        }
        if (!this.__callbacks.hasOwnProperty(event)) {
          this.__callbacks[event] = [];
        }
        this.__callbacks[event].push(callback);
      } else {
        throw new RangeError("Unable to add callback: Unknown event '" + event + "'");
      }
      return this;
    };

    RadioKitBase.prototype.off = function(event, callback) {
      var index;
      if (typeof event !== "string") {
        throw new TypeError("Unable to add callback: event name must be a string");
      }
      if (typeof callback !== "function") {
        throw new TypeError("Unable to remove callback: Callback must be a function");
      }
      if (indexOf.call(this.EVENTS, event) >= 0) {
        if (typeof this.__callbacks !== "object") {
          return;
        }
        index = this.__callbacks[event].indexOf(callback);
        if (index > -1) {
          this.__callbacks[event].splice(index, 1);
        } else {
          throw new RangeError("Unable to remove callback: Passed function is not registered as a callback");
        }
      } else {
        throw new RangeError("Unable to remove callback: Unknown event '" + event + "'");
      }
      return this;
    };

    RadioKitBase.prototype.trigger = function(event) {
      var callback, i, len, ref;
      if (typeof event !== "string") {
        throw new TypeError("Unable to add callback: event name must be a string");
      }
      if (indexOf.call(this.EVENTS, event) >= 0) {
        if (typeof this.__callbacks !== "object") {
          return;
        }
        if (this.__callbacks.hasOwnProperty(event)) {
          ref = this.__callbacks[event];
          for (i = 0, len = ref.length; i < len; i++) {
            callback = ref[i];
            callback.apply(this, arguments);
          }
        }
      } else {
        throw new RangeError("Unable to add callback: Unknown event '" + event + "'");
      }
      return this;
    };

    RadioKitBase.prototype.teardown = function() {
      var callbackList, event, ref;
      if (typeof this.__callbacks === "object") {
        ref = this.__callbacks;
        for (event in ref) {
          callbackList = ref[event];
          while (callbackList.length !== 0) {
            this.off(event, callbackList[0]);
          }
        }
      }
      return this;
    };

    RadioKitBase.prototype._validateConstructorOptions = function() {
      if (typeof this.options === "undefined") {
        throw new TypeError("Missing argument options in " + this.__proto__.constructor.name + " constructor");
      }
      if (typeof this.options !== "object") {
        throw new TypeError("Options passed to " + this.__proto__.constructor.name + " must be an object, " + (typeof this.options) + " given");
      }
    };

    RadioKitBase.prototype._validateConstructorOption = function(key, type, required, defaultValue) {
      var keySplit;
      if (typeof this.options === "object") {
        if (key.indexOf(".") !== -1) {
          keySplit = key.split(".", 2);
          if (!this.options.hasOwnProperty(keySplit[0])) {
            throw new RangeError("Options passed to " + this.__proto__.constructor.name + " must contain '" + keySplit[0] + "' key");
          }
          if (typeof this.options[keySplit[0]] !== "object") {
            throw new TypeError("Options passed to " + this.__proto__.constructor.name + " must contain '" + keySplit[0] + "' key that is an object, " + (typeof this.options[keySplit[0]]) + " given");
          }
          if (required === true) {
            if (!this.options[keySplit[0]].hasOwnProperty(keySplit[1])) {
              throw new RangeError("Options passed to " + this.__proto__.constructor.name + "." + keySplit[0] + " must contain '" + keySplit[1] + "' key");
            }
          }
          if (this.options[keySplit[0]].hasOwnProperty(keySplit[1])) {
            if (typeof this.options[keySplit[0]][keySplit[1]] === "undefined" && typeof defaultValue !== "undefined") {
              return this.options[keySplit[0]][keySplit[1]] = defaultValue;
            } else {
              if (typeof this.options[keySplit[0]][keySplit[1]] !== type) {
                throw new TypeError("Options passed to " + this.__proto__.constructor.name + "." + keySplit[0] + " must contain '" + keySplit[1] + "' key that is a " + type + ", " + (typeof this.options[keySplit[0]][keySplit[1]]) + " given");
              }
            }
          } else if (typeof defaultValue !== "undefined") {
            return this.options[keySplit[0]][keySplit[1]] = defaultValue;
          }
        } else {
          if (required === true) {
            if (this.options.hasOwnProperty(key) && typeof this.options[key] !== "undefined") {
              if (typeof this.options[key] === type) {

              } else {
                throw new TypeError("Options passed to " + this.__proto__.constructor.name + " must contain '" + key + "' key that is a " + type + ", " + (typeof this.options[key]) + " given");
              }
            } else {
              throw new TypeError("Options passed to " + this.__proto__.constructor.name + " must contain '" + key + "' key that is a " + type + ", no or undefined value was passed");
            }
          } else {
            if (this.options.hasOwnProperty(key) && typeof this.options[key] !== "undefined") {
              if (typeof this.options[key] === type) {

              } else {
                throw new TypeError("Options passed to " + this.__proto__.constructor.name + " must contain '" + key + "' key that is a " + type + ", " + (typeof this.options[key]) + " given");
              }
            } else {
              if (typeof defaultValue === "undefined") {

              } else {
                if (typeof defaultValue === type) {
                  return this.options[key] = defaultValue;
                }
              }
            }
          }
        }
      } else {
        throw new TypeError("Options passed to " + this.__proto__.constructor.name + " must be an object, " + (typeof this.options) + " given");
      }
    };

    return RadioKitBase;

  })();

  module.exports = RadioKitBase;

}).call(this);
