(function() {
  "use strict";
  var Immutable, RadioKitBase, RadioKitDataAjax, RadioKitDataQuery, RadioKitHelpersString, clone,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    slice = [].slice,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  Immutable = require('immutable');

  clone = require('clone');

  RadioKitBase = require('../base');

  RadioKitHelpersString = require('../helpers/string');

  RadioKitDataAjax = require('./ajax');

  RadioKitDataQuery = (function(superClass) {
    extend(RadioKitDataQuery, superClass);

    RadioKitDataQuery.prototype.CONDITIONS_OPERATORS = ["eq", "lt", "lte", "gt", "gte", "in", "isnull", "notnull", "deq"];

    RadioKitDataQuery.prototype.ORDER_DIRECTIONS = ["asc", "desc"];

    RadioKitDataQuery.prototype.EVENTS = ["fetch", "error", "abort"];

    function RadioKitDataQuery(options) {
      this.options = options;
      this.__onAjaxAbort = bind(this.__onAjaxAbort, this);
      this.__onAjaxError = bind(this.__onAjaxError, this);
      this.__onAjaxLoaded = bind(this.__onAjaxLoaded, this);
      this.__doAutoUpdate = bind(this.__doAutoUpdate, this);
      this.__scheduleAutoUpdate = bind(this.__scheduleAutoUpdate, this);
      this.isAutoUpdateEnabled = bind(this.isAutoUpdateEnabled, this);
      this.disableAutoUpdate = bind(this.disableAutoUpdate, this);
      this.enableAutoUpdate = bind(this.enableAutoUpdate, this);
      this.getData = bind(this.getData, this);
      this.fetch = bind(this.fetch, this);
      this.getCollectionParams = bind(this.getCollectionParams, this);
      this.getCollectionLocation = bind(this.getCollectionLocation, this);
      this.getCollectionUrl = bind(this.getCollectionUrl, this);
      this.method = bind(this.method, this);
      this.order = bind(this.order, this);
      this.first = bind(this.first, this);
      this.countTotal = bind(this.countTotal, this);
      this.getCountTotal = bind(this.getCountTotal, this);
      this.getOrder = bind(this.getOrder, this);
      this.getLimit = bind(this.getLimit, this);
      this.getOffset = bind(this.getOffset, this);
      this.getWhere = bind(this.getWhere, this);
      this.getScopes = bind(this.getScopes, this);
      this.getJoins = bind(this.getJoins, this);
      this.getSelect = bind(this.getSelect, this);
      this.offset = bind(this.offset, this);
      this.limit = bind(this.limit, this);
      this.scope = bind(this.scope, this);
      this.joins = bind(this.joins, this);
      this.where = bind(this.where, this);
      this.select = bind(this.select, this);
      this.teardown = bind(this.teardown, this);
      this.clone = bind(this.clone, this);
      this._validateConstructorOptions();
      this._validateConstructorOption("verbose", "boolean", false, false);
      this._validateConstructorOption("apiVersion", "string", false, "1.0");
      this._validateConstructorOption("appName", "string", true);
      this._validateConstructorOption("appConfig", "object", true);
      this._validateConstructorOption("auth", "object", true);
      this._validateConstructorOption("model", "string", true);
      this.__autoFetchEnabled = false;
      this.__autoFetchTimeoutId = null;
      this.__autoFetchInterval = null;
      this.__data = new Immutable.Seq().toIndexedSeq();
      this.__where = {};
      this.__joins = [];
      this.__scopes = [];
      this.__select = [];
      this.__offset = null;
      this.__limit = null;
      this.__orderParam = null;
      this.__orderDirection = null;
      this.__methodName = null;
      this.__countTotal = false;
      this.__ajax = new RadioKitDataAjax({
        "auth": this.options.auth,
        "verbose": this.options.verbose
      });
      this.__ajax.on("loaded", this.__onAjaxLoaded);
      this.__ajax.on("error", this.__onAjaxError);
      this.__ajax.on("abort", this.__onAjaxAbort);
    }

    RadioKitDataQuery.prototype.clone = function() {
      var obj;
      obj = new RadioKitDataQuery(this.options);
      obj.__select = clone(this.__select);
      obj.__where = clone(this.__where);
      obj.__joins = clone(this.__joins);
      obj.__scopes = clone(this.__scopes);
      obj.__limit = clone(this.__limit);
      obj.__offset = clone(this.__offset);
      obj.__orderParam = clone(this.__orderParam);
      obj.__orderDirection = clone(this.__orderDirection);
      obj.__countTotal = clone(this.__countTotal);
      obj.__data = this.__data;
      return obj;
    };

    RadioKitDataQuery.prototype.teardown = function() {
      RadioKitDataQuery.__super__.teardown.apply(this, arguments);
      this.disableAutoUpdate();
      this.__ajax.teardown();
      return this;
    };

    RadioKitDataQuery.prototype.select = function() {
      var select;
      select = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      this.__select = select;
      return this;
    };

    RadioKitDataQuery.prototype.where = function() {
      var operator, param, values;
      param = arguments[0], operator = arguments[1], values = 3 <= arguments.length ? slice.call(arguments, 2) : [];
      if (typeof param !== "string") {
        throw new TypeError("Unable to add conditions to the query: param name must be a string");
      }
      if (typeof operator !== "string") {
        throw new TypeError("Unable to add conditions to the query: operator name must be a string");
      }
      if (indexOf.call(this.CONDITIONS_OPERATORS, operator) < 0) {
        throw new RangeError("Unable to add conditions to the query: invalid operator '" + operator + "'");
      }
      if (!this.__where.hasOwnProperty(param)) {
        this.__where[param] = [];
      }
      this.__where[param].push({
        operator: operator,
        values: values
      });
      return this;
    };

    RadioKitDataQuery.prototype.joins = function(param) {
      this.__joins.push(param);
      return this;
    };

    RadioKitDataQuery.prototype.scope = function() {
      var param, values;
      param = arguments[0], values = 2 <= arguments.length ? slice.call(arguments, 1) : [];
      this.__scopes.push({
        param: param,
        "arguments": values
      });
      return this;
    };

    RadioKitDataQuery.prototype.limit = function(limit) {
      if (typeof limit !== "number") {
        throw new TypeError("limit must be a number");
      }
      this.__limit = limit;
      return this;
    };

    RadioKitDataQuery.prototype.offset = function(offset) {
      if (typeof offset !== "number") {
        throw new TypeError("offset must be a number");
      }
      this.__offset = offset;
      return this;
    };

    RadioKitDataQuery.prototype.getSelect = function() {
      return this.__select;
    };

    RadioKitDataQuery.prototype.getJoins = function() {
      return this.__joins;
    };

    RadioKitDataQuery.prototype.getScopes = function() {
      return this.__scopes;
    };

    RadioKitDataQuery.prototype.getWhere = function() {
      return this.__where;
    };

    RadioKitDataQuery.prototype.getOffset = function() {
      return this.__offset;
    };

    RadioKitDataQuery.prototype.getLimit = function() {
      return this.__limit;
    };

    RadioKitDataQuery.prototype.getOrder = function() {
      return [this.__orderParam, this.__orderDirection];
    };

    RadioKitDataQuery.prototype.getCountTotal = function() {
      return this.__countTotal;
    };

    RadioKitDataQuery.prototype.countTotal = function() {
      this.__countTotal = true;
      return this;
    };

    RadioKitDataQuery.prototype.first = function() {
      this.limit(0, 1);
      return this;
    };

    RadioKitDataQuery.prototype.order = function(param, direction) {
      if (indexOf.call(this.ORDER_DIRECTIONS, direction) < 0) {
        throw new RangeError("Invalid direction \"" + direction + "\"");
      }
      this.__orderParam = param;
      this.__orderDirection = direction;
      return this;
    };

    RadioKitDataQuery.prototype.method = function(methodName) {
      if (typeof methodName !== "string") {
        throw new TypeError("Method name must be a string, " + (typeof methodName) + " given");
      }
      this.__methodName = methodName;
      return this;
    };

    RadioKitDataQuery.prototype.getCollectionUrl = function() {
      return this.options.appConfig.baseUrl + this.getCollectionLocation() + "?" + this.getCollectionParams();
    };

    RadioKitDataQuery.prototype.getCollectionLocation = function() {
      if (this.__methodName === null) {
        return "/api/rest/v" + this.options.apiVersion + "/" + (RadioKitHelpersString.modelNameToUrl(this.options.model));
      } else {
        return "/api/rest/v" + this.options.apiVersion + "/" + (RadioKitHelpersString.modelNameToUrl(this.options.model)) + "/" + this.__methodName;
      }
    };

    RadioKitDataQuery.prototype.getCollectionParams = function() {
      var condition, conditionValues, conditions, i, j, k, l, len, len1, len2, len3, m, paramName, query, ref, ref1, ref2, ref3, scope;
      query = [];
      ref = this.__where;
      for (paramName in ref) {
        conditions = ref[paramName];
        for (j = 0, len = conditions.length; j < len; j++) {
          condition = conditions[j];
          conditionValues = condition.values;
          if (condition.operator === "isnull" || condition.operator === "notnull") {
            query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator));
          } else if (condition.operator === "deq") {
            query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator) + "%20" + encodeURIComponent(conditionValues[0]) + "%20" + encodeURIComponent(conditionValues[1]));
          } else {
            query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator) + "%20" + encodeURIComponent(conditionValues[0]));
          }
        }
      }
      ref1 = this.__select;
      for (k = 0, len1 = ref1.length; k < len1; k++) {
        paramName = ref1[k];
        query.push("a[]=" + encodeURIComponent(paramName));
      }
      ref2 = this.__joins;
      for (l = 0, len2 = ref2.length; l < len2; l++) {
        paramName = ref2[l];
        query.push("j[]=" + encodeURIComponent(paramName));
      }
      ref3 = this.__scopes;
      for (m = 0, len3 = ref3.length; m < len3; m++) {
        scope = ref3[m];
        if (scope["arguments"].length === 0) {
          query.push("s[" + scope.param + "]=");
        } else {
          i = 1;
          while (i < scope["arguments"].length) {
            query.push("s[" + scope.param + "][]=" + encodeURIComponent(scope["arguments"][i]));
            i = i + 1;
          }
        }
      }
      if ((this.__limit != null) && (this.__offset != null)) {
        query.push("l=" + this.__offset + "," + this.__limit);
      } else if (((this.__limit != null) && (this.__offset == null)) || ((this.__limit == null) && (this.__offset != null))) {
        throw new Error("If you set limit, you must set also offset and vice versa");
      }
      if ((this.__orderDirection != null) && (this.__orderParam != null)) {
        query.push("o=" + encodeURIComponent(this.__orderParam) + "," + this.__orderDirection);
      }
      if (this.__countTotal) {
        query.push("ct=1");
      }
      return query.join("&");
    };

    RadioKitDataQuery.prototype.fetch = function() {
      this.__ajax.get(this.getCollectionUrl());
      return this;
    };

    RadioKitDataQuery.prototype.getData = function() {
      return this.__data;
    };

    RadioKitDataQuery.prototype.enableAutoUpdate = function(interval) {
      if (this.__autoFetchEnabled) {
        return;
      }
      this.__autoFetchEnabled = true;
      if (typeof interval !== "number") {
        this.__autoFetchInterval = 1000;
      } else {
        this.__autoFetchInterval = interval;
      }
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Query] Enabled auto fetch for model " + this.options.model + ", interval " + this.__autoFetchInterval + " ms");
      }
      this.fetch();
      return this;
    };

    RadioKitDataQuery.prototype.disableAutoUpdate = function() {
      if (!this.__autoFetchEnabled) {
        return;
      }
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Query] Disabled auto fetch for model " + this.options.model);
      }
      this.__autoFetchEnabled = false;
      this.__autoFetchInterval = null;
      if (this.__autoFetchTimeoutId !== null) {
        clearTimeout(this.__autoFetchTimeoutId);
      }
      this.__autoFetchTimeoutId = null;
      return this;
    };

    RadioKitDataQuery.prototype.isAutoUpdateEnabled = function() {
      return this.__autoFetchEnabled;
    };

    RadioKitDataQuery.prototype.__scheduleAutoUpdate = function() {
      if (this.__autoFetchEnabled) {
        return this.__autoFetchTimeoutId = setTimeout(this.__doAutoUpdate, this.__autoFetchInterval);
      }
    };

    RadioKitDataQuery.prototype.__doAutoUpdate = function() {
      this.__autoFetchTimeoutId = null;
      return this.fetch();
    };

    RadioKitDataQuery.prototype.__onAjaxLoaded = function(eventName, status, response, countTotal) {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Query] Loaded data for model " + this.options.model);
      }
      if (response.data instanceof Array) {
        this.__data = Immutable.fromJS(response.data).toIndexedSeq();
      } else if (response.data instanceof Object) {
        this.__data = Immutable.fromJS([response.data]).toIndexedSeq();
      } else {
        console.warn("[RadioKit.Data.Query] Got unexpected data type " + (typeof response) + " while loading data for model " + this.options.model);
      }
      if (response.meta instanceof Object) {
        this.__meta = Immutable.fromJS(response.meta);
      }
      if (status >= 200 && status <= 299) {
        this.trigger("fetch", this, this.__data, this.__meta);
      } else {
        this.trigger("error", this);
      }
      return this.__scheduleAutoUpdate();
    };

    RadioKitDataQuery.prototype.__onAjaxError = function() {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Query] Error while loading data for model " + this.options.model);
      }
      this.trigger("error", this, this.options.model);
      return this.__scheduleAutoUpdate();
    };

    RadioKitDataQuery.prototype.__onAjaxAbort = function() {
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Query] Aborted while loading data for model " + this.options.model);
      }
      this.trigger("abort", this, this.options.model);
      return this.__scheduleAutoUpdate();
    };

    return RadioKitDataQuery;

  })(RadioKitBase);

  module.exports = RadioKitDataQuery;

}).call(this);
