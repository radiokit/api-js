(function() {
  "use strict";
  var RadioKitBase, RadioKitDataAjax,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RadioKitBase = require('../base');

  RadioKitDataAjax = (function(superClass) {
    extend(RadioKitDataAjax, superClass);

    RadioKitDataAjax.prototype.EVENTS = ["loading", "loaded", "abort", "error"];

    function RadioKitDataAjax(options) {
      this.options = options;
      this.__onAbort = bind(this.__onAbort, this);
      this.__onError = bind(this.__onError, this);
      this.__onProgress = bind(this.__onProgress, this);
      this.__onLoad = bind(this.__onLoad, this);
      this.__initializeRequest = bind(this.__initializeRequest, this);
      this.__openWithPayload = bind(this.__openWithPayload, this);
      this.__openWithoutPayload = bind(this.__openWithoutPayload, this);
      this.abort = bind(this.abort, this);
      this.patch = bind(this.patch, this);
      this.put = bind(this.put, this);
      this.post = bind(this.post, this);
      this["delete"] = bind(this["delete"], this);
      this.get = bind(this.get, this);
      this.teardown = bind(this.teardown, this);
      this._validateConstructorOptions();
      this._validateConstructorOption("verbose", "boolean", false, false);
      this._validateConstructorOption("auth", "object", true);
    }

    RadioKitDataAjax.prototype.teardown = function() {
      RadioKitDataAjax.__super__.teardown.apply(this, arguments);
      this.abort();
      return this;
    };

    RadioKitDataAjax.prototype.get = function(location) {
      this.__openWithoutPayload("get", location);
      return this;
    };

    RadioKitDataAjax.prototype["delete"] = function(location) {
      this.__openWithoutPayload("delete", location);
      return this;
    };

    RadioKitDataAjax.prototype.post = function(location, payload) {
      this.__openWithPayload("post", location, payload);
      return this;
    };

    RadioKitDataAjax.prototype.put = function(location, payload) {
      this.__openWithPayload("put", location, payload);
      return this;
    };

    RadioKitDataAjax.prototype.patch = function(location, payload) {
      this.__openWithPayload("patch", location, payload);
      return this;
    };

    RadioKitDataAjax.prototype.abort = function() {
      if (this.__isRunning) {
        if (this.options.verbose) {
          console.debug("[RadioKit.Data.Ajax] Aborting");
        }
        this.__request.removeEventListener("progress", this.__onProgress);
        this.__request.removeEventListener("load", this.__onLoad);
        this.__request.removeEventListener("abort", this.__onAbort);
        this.__request.removeEventListener("error", this.__onError);
        this.__request.abort();
        this.trigger("abort");
      }
      return this;
    };

    RadioKitDataAjax.prototype.__openWithoutPayload = function(method, location) {
      var k, ref, v;
      if (typeof location !== "string") {
        throw new TypeError("Unable to make " + (method.toUpperCase()) + " request: location must be a string");
      }
      this.__initializeRequest();
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Ajax] " + (method.toUpperCase()) + " " + location);
      }
      this.__request.open(method.toUpperCase(), location, true);
      this.__request.setRequestHeader("Accept", "application/json");
      ref = this.options.auth.getHeaders();
      for (k in ref) {
        v = ref[k];
        this.__request.setRequestHeader(k, v);
      }
      this.__isRunning = true;
      this.trigger("loading", method.toUpperCase(), location);
      this.__request.send();
      return this;
    };

    RadioKitDataAjax.prototype.__openWithPayload = function(method, location, payload) {
      var k, ref, v;
      if (typeof location !== "string") {
        throw new TypeError("Unable to make " + (method.toUpperCase()) + " request: location must be a string");
      }
      if (typeof payload !== "object") {
        throw new TypeError("Unable to make " + (method.toUpperCase()) + " request: payload must be an object");
      }
      this.__initializeRequest();
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Ajax] " + (method.toUpperCase()) + " " + location + " (" + (JSON.stringify(payload)) + ")");
      }
      this.__request.open(method.toUpperCase(), location, true);
      this.__request.setRequestHeader("Accept", "application/json");
      this.__request.setRequestHeader("Content-Type", "application/json");
      ref = this.options.auth.getHeaders();
      for (k in ref) {
        v = ref[k];
        this.__request.setRequestHeader(k, v);
      }
      this.__isRunning = true;
      this.trigger("loading", method.toUpperCase(), location, payload);
      this.__request.send(JSON.stringify(payload));
      return this;
    };

    RadioKitDataAjax.prototype.__initializeRequest = function() {
      if (this.__request !== null) {
        this.abort();
      }
      this.__isRunning = false;
      this.__request = new XMLHttpRequest();
      this.__request.addEventListener("progress", this.__onProgress, false);
      this.__request.addEventListener("load", this.__onLoad, false);
      this.__request.addEventListener("abort", this.__onAbort, false);
      this.__request.addEventListener("error", this.__onError, false);
      return this;
    };

    RadioKitDataAjax.prototype.__onLoad = function(e) {
      var responseJson;
      this.__isRunning = false;
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Ajax] Loaded data with status " + e.target.status);
      }
      responseJson = JSON.parse(e.target.responseText);
      return this.trigger("loaded", e.target.status, responseJson);
    };

    RadioKitDataAjax.prototype.__onProgress = function() {
      if (this.options.verbose) {
        return console.debug("[RadioKit.Data.Ajax] Loading data");
      }
    };

    RadioKitDataAjax.prototype.__onError = function() {
      this.__isRunning = false;
      console.warn("[RadioKit.Data.Ajax] Error while loading data");
      return this.trigger("error");
    };

    RadioKitDataAjax.prototype.__onAbort = function() {
      this.__isRunning = false;
      console.warn("[RadioKit.Data.Ajax] Aborted while loading data");
      return this.trigger("abort");
    };

    return RadioKitDataAjax;

  })(RadioKitBase);

  module.exports = RadioKitDataAjax;

}).call(this);
