(function() {
  "use strict";
  var RadioKitHelpersLocation;

  RadioKitHelpersLocation = (function() {
    function RadioKitHelpersLocation() {}

    RadioKitHelpersLocation.parseHash = function(hash) {
      var hashParam, hashParams, i, len, paramSplitted, paramSplittedKey, r;
      if (typeof hash !== "string") {
        throw new TypeError("Argument passed to parseHash must be a string");
      }
      r = {};
      hashParams = hash.split('&');
      for (i = 0, len = hashParams.length; i < len; i++) {
        hashParam = hashParams[i];
        paramSplitted = hashParam.split('=', 2);
        if (paramSplitted.length === 2) {
          paramSplittedKey = paramSplitted[0];
          if (paramSplittedKey[0] === "#") {
            paramSplittedKey = paramSplittedKey.substr(1);
          }
          r[paramSplittedKey] = decodeURIComponent(paramSplitted[1]);
        }
      }
      return r;
    };

    return RadioKitHelpersLocation;

  })();

  module.exports = RadioKitHelpersLocation;

}).call(this);
