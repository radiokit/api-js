(function() {
  "use strict";
  var RadioKitAuthEditorOAuth2, RadioKitBase, RadioKitDataAjax, RadioKitDataInterface, RadioKitDataQuery, RadioKitDataRecord,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RadioKitBase = require('../base');

  RadioKitDataQuery = require('./query');

  RadioKitDataRecord = require('./record');

  RadioKitDataAjax = require('./ajax');

  RadioKitAuthEditorOAuth2 = require('../auth/editor/oauth2');

  RadioKitDataInterface = (function(superClass) {
    extend(RadioKitDataInterface, superClass);

    RadioKitDataInterface.prototype.EVENTS = ["auth::success", "auth::failure"];

    function RadioKitDataInterface(options) {
      this.options = options;
      this.__onAuthFailure = bind(this.__onAuthFailure, this);
      this.__onAuthSuccess = bind(this.__onAuthSuccess, this);
      this.record = bind(this.record, this);
      this.query = bind(this.query, this);
      this.signOut = bind(this.signOut, this);
      this.signIn = bind(this.signIn, this);
      this.teardown = bind(this.teardown, this);
      this._validateConstructorOptions();
      this._validateConstructorOption("verbose", "boolean", false, false);
      this._validateConstructorOption("auth", "object", true);
      this._validateConstructorOption("auth.accessToken", "string", false);
      if (this.options.auth.accessToken == null) {
        this._validateConstructorOption("auth.clientId", "string", true);
      }
      this._validateConstructorOption("auth.baseUrl", "string", false);
      this._validateConstructorOption("apps", "object", true);
      this._validateConstructorOption("apiVersion", "string", false, "1.0");
      this.__queries = [];
      this.__records = {};
      this.options.auth = new RadioKitAuthEditorOAuth2(this.options.auth);
      this.options.auth.on("success", this.__onAuthSuccess);
      this.options.auth.on("failure", this.__onAuthFailure);
    }

    RadioKitDataInterface.prototype.teardown = function() {
      var i, len, query, ref;
      RadioKitDataInterface.__super__.teardown.apply(this, arguments);
      ref = this.__queries;
      for (i = 0, len = ref.length; i < len; i++) {
        query = ref[i];
        query.teardown();
      }
      this.__queries = [];
      return this;
    };

    RadioKitDataInterface.prototype.signIn = function(role) {
      if (typeof role !== "string") {
        throw new TypeError("Role identifier must be a string, " + (typeof role) + " given");
      }
      if (role !== "Editor") {
        throw new RangeError("Role identifier must be an 'Editor' (no other roles are supported at the moment)");
      }
      return this.options.auth.signIn();
    };

    RadioKitDataInterface.prototype.signOut = function() {
      return this.options.auth.signOut();
    };

    RadioKitDataInterface.prototype.query = function(appName, model) {
      var query;
      if (!this.options.apps.hasOwnProperty(appName)) {
        throw new RangeError("Application '" + appName + "' is unknown");
      }
      query = new RadioKitDataQuery({
        "auth": this.options.auth,
        "appName": appName,
        "appConfig": this.options.apps[appName],
        "model": model,
        "verbose": this.options.verbose,
        "apiVersion": this.options.apiVersion
      });
      this.__queries.push(query);
      if (this.options.verbose) {
        console.debug("[RadioKit.Data.Interface] Added query for " + appName + " / " + model);
      }
      return query;
    };

    RadioKitDataInterface.prototype.record = function(appName, model, recordId) {
      var record;
      if (!this.options.apps.hasOwnProperty(appName)) {
        throw new RangeError("Application '" + appName + "' is unknown");
      }
      record = new RadioKitDataRecord({
        "auth": this.options.auth,
        "appName": appName,
        "appConfig": this.options.apps[appName],
        "model": model,
        "recordId": recordId,
        "verbose": this.options.verbose,
        "apiVersion": this.options.apiVersion
      });
      if (!this.__records.hasOwnProperty(model)) {
        this.__records[model] = {};
      }
      if (this.__records[model].hasOwnProperty(recordId)) {
        if (this.hasRecord(model, recordId)) {
          return this.getRecord(model, recordId);
        }
        this.__records[model][recordId] = record;
      }
      return record;
    };

    RadioKitDataInterface.prototype.__onAuthSuccess = function(event, token) {
      return this.trigger("auth::success", token);
    };

    RadioKitDataInterface.prototype.__onAuthFailure = function(event, reason) {
      return this.trigger("auth::failure", reason);
    };

    return RadioKitDataInterface;

  })(RadioKitBase);

  module.exports = RadioKitDataInterface;

}).call(this);
