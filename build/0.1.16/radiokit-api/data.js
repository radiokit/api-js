(function() {
  "use strict";
  var DataAjax, DataInterface, DataQuery, DataRecord, DataUpload;

  DataAjax = require('./data/ajax');

  DataRecord = require('./data/record');

  DataQuery = require('./data/query');

  DataInterface = require('./data/interface');

  DataUpload = require('./data/upload');

  module.exports = {
    "buildRecordGlobalID": (function(_this) {
      return function(app, model, id) {
        return "record://" + encodeURIComponent(app) + "/" + encodeURIComponent(model) + "/" + encodeURIComponent(id);
      };
    })(this),
    "Ajax": DataAjax,
    "Record": DataRecord,
    "Query": DataQuery,
    "Interface": DataInterface,
    "Upload": DataUpload
  };

}).call(this);
