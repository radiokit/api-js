# RadioKit JS API

JavaScript API for RadioKit.

# Installation

## Development

* Clone repo
* Type `npm install`
* If you want to enable automatic testing, compiling & preview on a local HTTP server: type `npm start`
* If you want to just compile the scripts (once): type `npm run build`
* If you want to just compile the scripts but watch for changes: type `npm run watch`
* If you want to just run specs: type `npm run spec`
* Enjoy

# Authors

Marcin Lewandowski, Łukasz Odziewa

