"use strict"

RadioKitBase             = require('../base')
RadioKitDataQuery        = require('./query')
RadioKitDataRecord       = require('./record')
RadioKitDataAjax         = require('./ajax')
RadioKitDataUpload       = require('./upload')
RadioKitAuthEditorOAuth2 = require('../auth/editor/oauth2')

Immutable                = require('immutable')
ParseURI                 = require('parse-uri')


class RadioKitDataInterface extends RadioKitBase
  EVENTS: [ "auth::success", "auth::failure" ]


  constructor: (@options) ->
    @_validateConstructorOptions()
    @_validateConstructorOption "verbose",          "boolean", false, false
    @_validateConstructorOption "auth",             "object",  true

    @_validateConstructorOption "auth.accessToken", "string",  false
    unless @options.auth.accessToken?
      @_validateConstructorOption "auth.clientId",  "string",  true

    @_validateConstructorOption "auth.baseUrl",     "string",  false
    @_validateConstructorOption "apps",             "object",  true
    @_validateConstructorOption "apiVersion",       "string",  false, "1.0"

    # Defaults
    @__uploads = new Immutable.Set()
    @__queries = new Immutable.Set()
    @__records = new Immutable.Set()

    @options.auth = new RadioKitAuthEditorOAuth2 @options.auth

    @options.auth.on("success", @__onAuthSuccess)
    @options.auth.on("failure", @__onAuthFailure)


  teardown: =>
    super

    for query in @__queries
      query.teardown()

    for record in @__records
      record.teardown()

    for upload in @__uploads
      upload.teardown()

    @


  signIn: =>
    @options.auth.signIn()


  signOut: =>
    @options.auth.signOut()


  fromGlobalID: (globalID) =>
    uri = ParseURI(globalID)

    if uri.protocol == "record"
      recordIdParts = uri.path.split("/", 3)
      @record(uri.host, recordIdParts[1], recordIdParts[2])

    else
      throw new RangeError("Unknown type of global ID")

  query: (appName, model) =>
    throw new RangeError("Application '#{appName}' is unknown") unless @options.apps.hasOwnProperty(appName)

    query = new RadioKitDataQuery
      "auth"          : @options.auth
      "appName"       : appName
      "appConfig"     : @options.apps[appName]
      "model"         : model
      "verbose"       : @options.verbose
      "apiVersion"    : @options.apiVersion

    query.on "teardown", @__onQueryTeardown
    @__queries.add query

    console.debug("[RadioKit.Data.Interface] Added query for #{appName} / #{model}") if @options.verbose

    query


  record: (appName, model, recordId) =>
    throw new RangeError("Application '#{appName}' is unknown") unless @options.apps.hasOwnProperty(appName)

    record = new RadioKitDataRecord
      "auth"          : @options.auth
      "appName"       : appName
      "appConfig"     : @options.apps[appName]
      "model"         : model
      "recordId"      : recordId
      "verbose"       : @options.verbose
      "apiVersion"    : @options.apiVersion

    record.on "teardown", @__onRecordTeardown
    @__records.add record

    console.debug("[RadioKit.Data.Interface] Added record for #{appName} / #{model} / #{recordId}") if @options.verbose

    record


  upload: (recordRepositoryId, uploadOptions) =>
    throw new RangeError("Application 'vault' is unknown, you must define configuration for 'vault' app if you want to use uploader") unless @options.apps.hasOwnProperty("vault")
    throw new TypeError("Record Repository ID must be a string, #{typeof(recordRepositoryId)} given") unless typeof(recordRepositoryId) == "string"

    upload = new RadioKitDataUpload
      "auth"               : @options.auth
      "vaultConfig"        : @options.apps.vault
      "verbose"            : @options.verbose
      "apiVersion"         : @options.apiVersion
      "recordRepositoryId" : recordRepositoryId
      "maxFiles"           : uploadOptions.maxFiles
      "autoStart"          : uploadOptions.autoStart

    upload.on "teardown", @__onUploadTeardown
    @__uploads.add upload

    console.debug("[RadioKit.Data.Interface] Added upload for #{recordRepositoryId}") if @options.verbose

    upload


  getQueries: =>
    @__queries


  getRecords: =>
    @__records


  getUploads: =>
    @__uploads


  __onAuthSuccess: (event, redirect) =>
    @trigger("auth::success", redirect)


  __onAuthFailure: (event, reason) =>
    @trigger("auth::failure", reason)


  __onUploadTeardown: (event, upload) =>
    console.debug("[RadioKit.Data.Interface] Removed upload for #{upload.options.recordRepositoryId}") if @options.verbose
    @__uploads.delete upload


  __onQueryTeardown: (event, query) =>
    console.debug("[RadioKit.Data.Interface] Removed query for #{query.options.appName} / #{query.options.model}") if @options.verbose
    @__queries.delete query


  __onRecordTeardown: (event, record) =>
    console.debug("[RadioKit.Data.Interface] Removed record for #{record.options.appName} / #{record.options.model} / #{record.options.recordId}") if @options.verbose
    @__records.delete record


module.exports = RadioKitDataInterface
