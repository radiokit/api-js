"use strict"

RadioKitBase = require('../base')

class RadioKitDataAjax extends RadioKitBase
  EVENTS: [ "loading", "loaded", "abort", "error" ]

  constructor: (@options) ->
    @_validateConstructorOptions()
    @_validateConstructorOption "verbose",       "boolean", false, false
    @_validateConstructorOption "auth",          "object",  true


  teardown: =>
    super

    @abort()
    @


  get: (location) =>
    @__openWithoutPayload("get", location)
    @


  delete: (location) =>
    @__openWithoutPayload("delete", location)
    @


  post: (location, payload) =>
    @__openWithPayload("post", location, payload)
    @


  put: (location, payload) =>
    @__openWithPayload("put", location, payload)
    @


  patch: (location, payload) =>
    @__openWithPayload("patch", location, payload)
    @


  abort: =>
    if @__isRunning
      console.debug("[RadioKit.Data.Ajax] Aborting") if @options.verbose
      @__request.removeEventListener "progress", @__onProgress
      @__request.removeEventListener "load",     @__onLoad
      @__request.removeEventListener "abort",    @__onAbort
      @__request.removeEventListener "error",    @__onError

      @__request.abort()

      @trigger("abort")
    @


  __openWithoutPayload: (method, location) =>
    throw new TypeError("Unable to make #{method.toUpperCase()} request: location must be a string") unless typeof(location) == "string"

    @__initializeRequest()
    console.debug("[RadioKit.Data.Ajax] #{method.toUpperCase()} #{location}") if @options.verbose
    @__request.open(method.toUpperCase(), location, true)
    @__request.setRequestHeader("Accept", "application/json")
    for k, v of @options.auth.getHeaders()
      @__request.setRequestHeader(k, v)
    @__isRunning = true
    @trigger("loading", method.toUpperCase(), location)
    @__request.send()
    @


  __openWithPayload: (method, location, payload) =>
    throw new TypeError("Unable to make #{method.toUpperCase()} request: location must be a string") unless typeof(location) == "string"
    throw new TypeError("Unable to make #{method.toUpperCase()} request: payload must be an object") unless typeof(payload) == "object"

    @__initializeRequest()
    console.debug("[RadioKit.Data.Ajax] #{method.toUpperCase()} #{location} (#{JSON.stringify(payload)})") if @options.verbose
    @__request.open(method.toUpperCase(), location, true)
    @__request.setRequestHeader("Accept", "application/json")
    @__request.setRequestHeader("Content-Type", "application/json")
    for k, v of @options.auth.getHeaders()
      @__request.setRequestHeader(k, v)
    @__isRunning = true
    @trigger("loading", method.toUpperCase(), location, payload)
    @__request.send(JSON.stringify(payload))
    @


  __initializeRequest: =>
    if @__request != null
      @abort()

    @__isRunning = false
    @__request   = new XMLHttpRequest()
    @__request.addEventListener "progress", @__onProgress, false
    @__request.addEventListener "load",     @__onLoad,     false
    @__request.addEventListener "abort",    @__onAbort,    false
    @__request.addEventListener "error",    @__onError,    false
    @


  __onLoad: (e) =>
    @__isRunning = false
    console.debug("[RadioKit.Data.Ajax] Loaded data with status #{e.target.status}") if @options.verbose
    responseJson = JSON.parse(e.target.responseText)
    @trigger("loaded", e.target.status, responseJson)
      

  __onProgress: =>
    console.debug("[RadioKit.Data.Ajax] Loading data") if @options.verbose


  __onError: =>
    @__isRunning = false
    console.warn("[RadioKit.Data.Ajax] Error while loading data")
    @trigger("error")


  __onAbort: =>
    @__isRunning = false
    console.warn("[RadioKit.Data.Ajax] Aborted while loading data")
    @trigger("abort")


module.exports = RadioKitDataAjax
