"use strict"

Immutable              = require('immutable')
clone                  = require('clone')

RadioKitBase           = require('../base')
RadioKitHelpersString  = require('../helpers/string')
RadioKitDataAjax       = require('./ajax')

class RadioKitDataQuery extends RadioKitBase
  CONDITIONS_OPERATORS: [ "eq", "lt", "lte", "gt", "gte", "in", "isnull", "notnull", "deq", "din", "like", "ilike" ]
  ORDER_DIRECTIONS: [ "asc", "desc" ]
  EVENTS: [ "fetch", "error", "abort" ]

  constructor: (@options) ->
    @_validateConstructorOptions()
    @_validateConstructorOption "verbose",       "boolean", false, false
    @_validateConstructorOption "apiVersion",    "string",  false, "1.0"
    @_validateConstructorOption "appName",       "string",  true
    @_validateConstructorOption "appConfig",     "object",  true
    @_validateConstructorOption "auth",          "object",  true
    @_validateConstructorOption "model",         "string",  true

    @__autoFetchEnabled   = false
    @__autoFetchTimeoutId = null
    @__autoFetchInterval  = null
    @__data               = new Immutable.Seq().toIndexedSeq()
    @__where              = {}
    @__joins              = []
    @__scopes             = []
    @__select             = []
    @__offset             = null
    @__limit              = null
    @__order              = []
    @__methodName         = null # DEPRECATED
    @__countTotal         = false

    @__ajax = new RadioKitDataAjax
      "auth"          : @options.auth
      "verbose"       : @options.verbose

    @__ajax.on("loaded", @__onAjaxLoaded)
    @__ajax.on("error", @__onAjaxError)
    @__ajax.on("abort", @__onAjaxAbort)


  clone: =>
    obj = new RadioKitDataQuery(@options)


    obj.__select         = clone(@__select)
    obj.__order          = clone(@__order)
    obj.__where          = clone(@__where)
    obj.__joins          = clone(@__joins)
    obj.__scopes         = clone(@__scopes)
    obj.__limit          = clone(@__limit)
    obj.__offset         = clone(@__offset)
    obj.__countTotal     = clone(@__countTotal)
    obj.__data           = @__data

    obj


  teardown: =>
    super

    @disableAutoUpdate()
    @__ajax.teardown()
    @


  select: (select...) =>
    @__select = @__select.concat(select)
    @


  clearSelect: =>
    @__select = []
    @


  where: (param, operator, values...) =>
    throw new TypeError("Unable to add conditions to the query: param name must be a string") unless typeof(param) == "string"
    throw new TypeError("Unable to add conditions to the query: operator name must be a string") unless typeof(operator) == "string"
    throw new RangeError("Unable to add conditions to the query: invalid operator '#{operator}'") unless operator in @CONDITIONS_OPERATORS

    @__where[param] = [] unless @__where.hasOwnProperty(param)
    @__where[param].push({ operator: operator, values: values })
    @


  clearWhere: =>
    @__where = {}
    @


  order: (param, direction) =>
    throw new RangeError("Invalid direction \"" + direction + "\"") unless direction in @ORDER_DIRECTIONS
    @__order.push([param, direction])
    @


  clearOrder: =>
    @__order = []
    @


  method: (methodName) =>
    throw new TypeError("Method name must be a string, #{typeof(methodName)} given") unless typeof(methodName) == "string"
    @__methodName = methodName
    @


  joins: (param) =>
    @__joins.push param
    @


  clearJoins: =>
    @__joins = []
    @


  scope: (param, values...) =>
    @__scopes.push
      param: param
      arguments: values
    @


  clearScopes: =>
    @__scopes = []
    @


  limit: (limit) =>
    throw new TypeError("limit must be a number") unless typeof(limit) == "number"
    @__limit = limit
    @


  clearLimit: =>
    @__limit = null
    @


  offset: (offset) =>
    throw new TypeError("offset must be a number") unless typeof(offset) == "number"
    @__offset = offset
    @


  clearOffset: =>
    @__offset = null
    @


  getSelect: =>
    @__select


  getJoins: =>
    @__joins


  getScopes: =>
    @__scopes


  getWhere: =>
    @__where


  getOffset: =>
    @__offset


  getLimit: =>
    @__limit


  getOrder: =>
    @__order


  getCountTotal: =>
    @__countTotal


  countTotal: =>
    @__countTotal = true
    @


  first: =>
    @limit(0, 1)
    @


  getCollectionUrl: =>
    @options.appConfig.baseUrl + @getCollectionLocation() + "?" + @getCollectionParams()


  getCollectionLocation: =>
    if @__methodName == null
      "/api/rest/v#{@options.apiVersion}/#{RadioKitHelpersString.modelNameToUrl(@options.model)}"
    else
      "/api/rest/v#{@options.apiVersion}/#{RadioKitHelpersString.modelNameToUrl(@options.model)}/#{@__methodName}"


  getCollectionParams: =>
    query = []

    for paramName, conditions of @__where
      for condition in conditions
        # if typeof(condition.value) == "function" # FIXME
          # conditionValue = condition.value(@, paramName, condition.operator)
        # else

        conditionValues = condition.values

        if condition.operator == "isnull" or condition.operator == "notnull"
          query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator))
        else if condition.operator == "deq"
          query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator) + "%20" + encodeURIComponent(conditionValues[0]) + "%20" + encodeURIComponent(conditionValues[1]))
        else if condition.operator == "in" or condition.operator == "din"
          if conditionValues.length >= 1 and conditionValues[0].constructor == Array
            query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator) + "%20" + conditionValues[0].map((x) => encodeURIComponent(x)).join("%20"))
          else
            query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator) + "%20" + conditionValues.map((x) => encodeURIComponent(x)).join("%20"))

        else
          query.push("c[" + encodeURIComponent(paramName) + "][]=" + encodeURIComponent(condition.operator) + "%20" + encodeURIComponent(conditionValues[0]))

    for paramName in @__select
      query.push("a[]=" + encodeURIComponent(paramName))

    for paramName in @__joins
      query.push("j[]=" + encodeURIComponent(paramName))

    if @__scopes
      for scope in @__scopes
        if scope.arguments.length != 0
          query.push("s[]=" + encodeURIComponent(scope.param) + "%20" + scope.arguments.map((x) => encodeURIComponent(x)).join("%20"))
        else
          query.push("s[]=" + encodeURIComponent(scope.param))

    if @__limit? and @__offset?
      query.push "l=" + @__offset + "," + @__limit
    else if (@__limit? and not @__offset?) or (not @__limit? and @__offset?)
      throw new Error("If you set limit, you must set also offset and vice versa")

    for order in @__order
      query.push "o[]=" + encodeURIComponent(order[0]) + "%20" + encodeURIComponent(order[1])

    query.push "ct=1" if @__countTotal

    query.join("&")


  fetch: =>
    @__ajax.get(@getCollectionUrl())

    @


  getData: =>
    @__data


  enableAutoUpdate: (interval) =>
    return if @__autoFetchEnabled

    @__autoFetchEnabled = true

    if typeof(interval) != "number"
      @__autoFetchInterval = 1000
    else
      @__autoFetchInterval = interval

    console.debug("[RadioKit.Data.Query] Enabled auto fetch for model #{@options.model}, interval #{@__autoFetchInterval} ms") if @options.verbose
    @fetch()
    @


  disableAutoUpdate: =>
    return unless @__autoFetchEnabled
    console.debug("[RadioKit.Data.Query] Disabled auto fetch for model #{@options.model}") if @options.verbose

    @__autoFetchEnabled = false
    @__autoFetchInterval = null
    clearTimeout(@__autoFetchTimeoutId) unless @__autoFetchTimeoutId == null
    @__autoFetchTimeoutId = null
    @


  isAutoUpdateEnabled: =>
    @__autoFetchEnabled


  __scheduleAutoUpdate: =>
    @__autoFetchTimeoutId = setTimeout(@__doAutoUpdate, @__autoFetchInterval) if @__autoFetchEnabled


  __doAutoUpdate: =>
    @__autoFetchTimeoutId = null
    @fetch()


  __onAjaxLoaded: (eventName, status, response, countTotal) =>
    console.debug("[RadioKit.Data.Query] Loaded data for model #{@options.model}") if @options.verbose
    if response.data instanceof Array
      @__data = Immutable.fromJS(response.data).toIndexedSeq()

    else if response.data instanceof Object
      @__data = Immutable.fromJS([response.data]).toIndexedSeq()

    else
      console.warn("[RadioKit.Data.Query] Got unexpected data type #{typeof(response)} while loading data for model #{@options.model}")

    if response.meta instanceof Object
      @__meta = Immutable.fromJS(response.meta)

    if status >= 200 and status <= 299
      @trigger("fetch", @, @__data, @__meta)
    else
      @trigger("error", @)

    @__scheduleAutoUpdate()


  __onAjaxError: =>
    console.debug("[RadioKit.Data.Query] Error while loading data for model #{@options.model}") if @options.verbose
    @trigger("error", @, @options.model)
    @__scheduleAutoUpdate()


  __onAjaxAbort: =>
    console.debug("[RadioKit.Data.Query] Aborted while loading data for model #{@options.model}") if @options.verbose
    @trigger("abort", @, @options.model)
    @__scheduleAutoUpdate()


module.exports = RadioKitDataQuery
