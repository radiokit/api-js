"use strict"

RadioKitBase           = require('../base')
RadioKitHelpersString  = require('../helpers/string')
RadioKitDataAjax       = require('./ajax')

Immutable              = require('immutable')

class RadioKitDataRecord extends RadioKitBase
  EVENTS: [ "loading", "loaded", "abort", "warning", "error" ]


  constructor: (@options) ->
    @_validateConstructorOptions()
    @_validateConstructorOption "recordId",      "string",  false
    @_validateConstructorOption "verbose",       "boolean", false, false
    @_validateConstructorOption "apiVersion",    "string",  false, "1.0"
    @_validateConstructorOption "appName",       "string",  true
    @_validateConstructorOption "appConfig",     "object",  true
    @_validateConstructorOption "auth",          "object",  true
    @_validateConstructorOption "model",         "string",  true

    @__ajax = new RadioKitDataAjax
      "auth"          : @options.auth
      "verbose"       : @options.verbose

    @__ajax.on("loading", @__onAjaxLoading)
    @__ajax.on("loaded", @__onAjaxLoaded)
    @__ajax.on("error", @__onAjaxError)
    @__ajax.on("abort", @__onAjaxAbort)


  teardown: =>
    super

    @__ajax.teardown()
    @


  getRecordLocation: =>
    if @options.recordId
      @options.appConfig.baseUrl + "/api/rest/v#{@options.apiVersion}/#{RadioKitHelpersString.modelNameToUrl(@options.model)}/#{@options.recordId}"
    else
      @options.appConfig.baseUrl + "/api/rest/v#{@options.apiVersion}/#{RadioKitHelpersString.modelNameToUrl(@options.model)}"



  show: =>
    console.warn("RadioKit.Data.Interface#show is deprecated, use RadioKit.Data.Interface#read instead")
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key if you want to use show()") unless @options.recordId
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key that is a string if you want to use show(), #{typeof(options.recordId)} given") unless typeof(@options.recordId) == "string"
    console.debug("[RadioKit.Data.Record] Showing model #{@options.model}##{@options.recordId}") if @options.verbose
    @__ajax.get(@getRecordLocation())
    @

  read: =>
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key if you want to use show()") unless @options.recordId
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key that is a string if you want to use show(), #{typeof(options.recordId)} given") unless typeof(@options.recordId) == "string"
    console.debug("[RadioKit.Data.Record] Showing model #{@options.model}##{@options.recordId}") if @options.verbose
    @__ajax.get(@getRecordLocation())
    @


  create: (attributes) =>
    throw new RangeError("Options passed to RadioKit.Data.Record must not contain recordId key if you want to use create()") if @options.recordId

    console.debug("[RadioKit.Data.Record] Creating model #{@options.model} with attributes #{JSON.stringify(attributes)}") if @options.verbose
    @__ajax.post(@getRecordLocation(), attributes)
    @


  update: (attributes) =>
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key if you want to use update()") unless @options.recordId
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key that is a string if you want to use update(), #{typeof(options.recordId)} given") unless typeof(@options.recordId) == "string"
    console.debug("[RadioKit.Data.Record] Updating model #{@options.model}##{@options.recordId} with attributes #{JSON.stringify(attributes)}") if @options.verbose

    @__ajax.patch(@getRecordLocation(), attributes)
    @


  destroy: =>
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key if you want to use destroy()") unless @options.recordId
    throw new RangeError("Options passed to RadioKit.Data.Record must contain recordId key that is a string if you want to use destroy(), #{typeof(options.recordId)} given") unless typeof(@options.recordId) == "string"
    console.debug("[RadioKit.Data.Record] Destroying model #{@options.model}##{@options.recordId}") if @options.verbose
    @__ajax.delete(@getRecordLocation())
    @



  getRecords: =>
    throw new Error("Please fetch the data first") if @__records == null
    @__records


  __onAjaxLoading: (eventName) =>
    console.debug("[RadioKit.Data.Record] Starting AJAX request for model #{@options.model}##{@options.recordId}") if @options.verbose
    @trigger("loading", @)


  __onAjaxLoaded: (eventName, status, response) =>
    console.debug("[RadioKit.Data.Record] Finished AJAX request for model #{@options.model}##{@options.recordId} with status #{status}, response #{JSON.stringify(response)}") if @options.verbose
    if status >= 200 and status <= 299
      @trigger("loaded", @, Immutable.fromJS(response.data))
      # TODO setup recordId for future usage
    # TODO handle unauthorized
    # TODO handle forbidden
    else if status == 422
      @trigger("warning", @)
    else
      @trigger("error", @)


  __onAjaxError: =>
    console.warn("[RadioKit.Data.Record] Error during AJAX request for model #{@options.model}##{@options.recordId}")
    @trigger("error", @)


  __onAjaxAbort: =>
    console.warn("[RadioKit.Data.Record] Aborted AJAX request for model #{@options.model}##{@options.recordId}")
    @trigger("abort", @)


module.exports = RadioKitDataRecord
