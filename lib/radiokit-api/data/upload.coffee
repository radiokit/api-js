"use strict"

RadioKitBase           = require('../base')

Resumable              = require('resumablejs')
Immutable              = require('immutable')

class RadioKitDataUpload extends RadioKitBase
  EVENTS: [ "added", "progress", "retry", "error", "started", "cancelled", "completed" ]

  constructor: (@options) ->
    @_validateConstructorOptions()
    @_validateConstructorOption "verbose",            "boolean", false, false
    @_validateConstructorOption "apiVersion",         "string",  false, "1.0"
    @_validateConstructorOption "vaultConfig",        "object",  true
    @_validateConstructorOption "auth",               "object",  true
    @_validateConstructorOption "recordRepositoryId", "string",  true
    @_validateConstructorOption "maxFiles",           "number",  false
    @_validateConstructorOption "autoStart",          "boolean", false, false


    @__resumable = new Resumable
      testChunks:               false
      forceChunkSize:           true
      chunkSize:                2097152 # 2 GB
      target:                   @options.vaultConfig.baseUrl + "/api/upload/v" + @options.apiVersion + "/resumablejs"
      headers:                  @options.auth.getHeaders()
      query:                    { record_repository_id: @options.recordRepositoryId }
      simultaneousUploads:      1
      minFileSize:              1
      maxFiles:                 @options.maxFiles
      generateUniqueIdentifier: @__generateUniqueIdentifier


    # TODO fallback to non-chunked
    throw new Error("Chunked uploading is not supported on this browser") unless @__resumable.support

    @__resumable.on "fileAdded",    @__onResumableFileAdded
    @__resumable.on "fileProgress", @__onResumableFileProgress
    @__resumable.on "fileRetry",    @__onResumableFileRetry
    @__resumable.on "fileError",    @__onResumableFileError
    @__resumable.on "complete",     @__onResumableCompleted
    @__resumable.on "uploadStart",  @__onResumableUploadStarted
    @__resumable.on "beforeCancel", @__onResumableUploadCancelled


  teardown: =>
    super
    @__resumable.cancel()
    delete @__resumable
    @


  assignBrowse: (domNode) =>
    @__resumable.assignBrowse(domNode)
    @


  assignDrop: (domNode) =>
    @__resumable.assignDrop(domNode)
    @


  start: =>
    @__resumable.upload()


  cancel: =>
    @__resumable.cancel()


  getQueue: =>
    files = @__resumable.files.map (file) =>
      {
        id: file.uniqueIdentifier,
        name: file.fileName,
        size: file.size,
        progress: parseInt(file.progress() * 100),
        uploading: file.isUploading(),
        completed: file.isComplete()
      }

    Immutable.fromJS(files)


  __onResumableFileAdded: =>
    @trigger("added", @)
    @start() if @options.autoStart


  __onResumableFileRetry: =>
    @trigger("retry", @)


  __onResumableFileError: =>
    @trigger("error", @)


  __onResumableFileProgress: =>
    @trigger("progress", @)


  __onResumableCompleted: =>
    @trigger("completed", @)


  __onResumableUploadStarted: =>
    @trigger("started", @)


  __onResumableUploadCancelled: =>
    @trigger("cancelled", @)


  __generateUniqueIdentifier: =>
    @options.recordRepositoryId + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + parseInt(Math.random() * 10000000000000000) + Date.now()


module.exports = RadioKitDataUpload
