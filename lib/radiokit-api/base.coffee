"use strict"

class RadioKitBase
  on: (event, callback) =>
    throw new TypeError("Unable to add callback: event name must be a string") unless typeof(event) == "string"
    throw new TypeError("Unable to add callback: Callback must be a function") unless typeof(callback) == "function"

    if event == "teardown" or event in @EVENTS
      @__callbacks = {} unless typeof(@__callbacks) == "object"
      @__callbacks[event] = [] unless @__callbacks.hasOwnProperty(event)
      @__callbacks[event].push(callback)
    else
      throw new RangeError("Unable to add callback: Unknown event '#{event}'")

    @


  off: (event, callback) =>
    throw new TypeError("Unable to add callback: event name must be a string") unless typeof(event) == "string"
    throw new TypeError("Unable to remove callback: Callback must be a function") unless typeof(callback) == "function"

    if event == "teardown" or event in @EVENTS
      return unless typeof(@__callbacks) == "object"
      index = @__callbacks[event].indexOf(callback)
      if index > -1
        @__callbacks[event].splice(index, 1)
      else
        throw new RangeError("Unable to remove callback: Passed function is not registered as a callback")
    else
      throw new RangeError("Unable to remove callback: Unknown event '#{event}'")

    @


  trigger: (event) =>
    throw new TypeError("Unable to add callback: event name must be a string") unless typeof(event) == "string"

    if event == "teardown" or event in @EVENTS
      return unless typeof(@__callbacks) == "object"
      if @__callbacks.hasOwnProperty(event)
        for callback in @__callbacks[event]
          callback.apply(@, arguments)
    else
      throw new RangeError("Unable to add callback: Unknown event '#{event}'")

    @


  teardown: =>
    @trigger "teardown", @
    if typeof(@__callbacks) == "object"
      for event, callbackList of @__callbacks
        until callbackList.length == 0
          @off(event, callbackList[0])

    @



  _validateConstructorOptions: =>
    if typeof(@options) == "undefined"
      throw new TypeError("Missing argument options in #{@.__proto__.constructor.name} constructor")

    unless typeof(@options) == "object"
      throw new TypeError("Options passed to #{@.__proto__.constructor.name} must be an object, #{typeof(@options)} given")


  _validateConstructorOption: (key, type, required, defaultValue) =>
    if typeof(@options) == "object"
      if key.indexOf(".") != -1
        keySplit = key.split(".", 2)
        throw new RangeError("Options passed to #{@.__proto__.constructor.name} must contain '#{keySplit[0]}' key") unless @options.hasOwnProperty(keySplit[0])
        throw new TypeError("Options passed to #{@.__proto__.constructor.name} must contain '#{keySplit[0]}' key that is an object, #{typeof(@options[keySplit[0]])} given") unless typeof(@options[keySplit[0]]) == "object"

        if required == true
          throw new RangeError("Options passed to #{@.__proto__.constructor.name}.#{keySplit[0]} must contain '#{keySplit[1]}' key") unless @options[keySplit[0]].hasOwnProperty(keySplit[1])

        if @options[keySplit[0]].hasOwnProperty(keySplit[1])
          if typeof(@options[keySplit[0]][keySplit[1]]) == "undefined" and typeof(defaultValue) != "undefined"
            @options[keySplit[0]][keySplit[1]] = defaultValue
          else
            throw new TypeError("Options passed to #{@.__proto__.constructor.name}.#{keySplit[0]} must contain '#{keySplit[1]}' key that is a #{type}, #{typeof(@options[keySplit[0]][keySplit[1]])} given") unless typeof(@options[keySplit[0]][keySplit[1]]) == type

        else if typeof(defaultValue) != "undefined"
          @options[keySplit[0]][keySplit[1]] = defaultValue

      else
        if required == true
          if @options.hasOwnProperty(key) and typeof(@options[key]) != "undefined"
            if typeof(@options[key]) == type
              # OK
            else
              throw new TypeError("Options passed to #{@.__proto__.constructor.name} must contain '#{key}' key that is a #{type}, #{typeof(@options[key])} given")

          else
            throw new TypeError("Options passed to #{@.__proto__.constructor.name} must contain '#{key}' key that is a #{type}, no or undefined value was passed")

        else
          if @options.hasOwnProperty(key) and typeof(@options[key]) != "undefined"
            if typeof(@options[key]) == type
              # OK
            else
              throw new TypeError("Options passed to #{@.__proto__.constructor.name} must contain '#{key}' key that is a #{type}, #{typeof(@options[key])} given")

          else
            if typeof(defaultValue) == "undefined"
              # OK
            else
              if typeof(defaultValue) == type
                @options[key] = defaultValue

    else
      throw new TypeError("Options passed to #{@.__proto__.constructor.name} must be an object, #{typeof(@options)} given")


module.exports = RadioKitBase
