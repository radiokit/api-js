"use strict"

DataAjax       = require('./data/ajax')
DataRecord     = require('./data/record')
DataQuery      = require('./data/query')
DataInterface  = require('./data/interface')
DataUpload     = require('./data/upload')


module.exports = {
  "buildRecordGlobalID": (app, model, id) =>
    return "record://" + encodeURIComponent(app) + "/" + encodeURIComponent(model) + "/" + encodeURIComponent(id)

  "Ajax": DataAjax,
  "Record": DataRecord,
  "Query": DataQuery,
  "Interface": DataInterface,
  "Upload": DataUpload
}
