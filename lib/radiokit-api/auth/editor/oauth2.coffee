"use strict"

RadioKitHelpersLocation = require('../../helpers/location')
RadioKitAuthEditorBase  = require('./base')

class RadioKitAuthEditorOAuth2 extends RadioKitAuthEditorBase
  constructor: (@options) ->
    super

    @_validateConstructorOptions()
    @_validateConstructorOption "accessToken",   "string", false

    if @options.accessToken?
      @_authenticate { "Authorization": "Bearer #{@options.accessToken}" }

    else
      @_validateConstructorOption "clientId", "string", true
      @_validateConstructorOption "baseUrl" , "string", false, "https://auth.radiokitapp.org"


  signIn: =>
    if window?
      hashParams = RadioKitHelpersLocation.parseHash(window.location.hash)

      if hashParams.hasOwnProperty("error")
        @trigger "failure", hashParams.error
        return

      if hashParams.hasOwnProperty("access_token") and hashParams.hasOwnProperty("token_type") and hashParams.hasOwnProperty("state")
        stateSplitted = hashParams["state"].split("|", 2)
        stateSessionId = stateSplitted[0]
        stateRedirect = stateSplitted[1]

        # Important: ensure that first character of redirect is / so we support only
        # local redirects.
        if stateSessionId == window.sessionStorage["localSessionId"] and stateRedirect[0] == "/"
          # Important: authenticate first, then trigger, because triggered callback
          # can rely on the fact we are already authenticated and e.g. issue AJAX call
          @_authenticate { "Authorization": "#{hashParams.token_type} #{hashParams.access_token}" }

          @trigger "success", stateRedirect
          return

        else
          @trigger "failure", "invalid_state"
          return

      # TODO support expires_in


      if window.sessionStorage["localSessionId"]
        localSessionId = window.sessionStorage["localSessionId"]

      else
        localSessionId = (Math.random() * 10000000000000000).toString().replace(".", "")
        window.sessionStorage["localSessionId"] = localSessionId

      state = localSessionId + "|" + window.location.pathname + window.location.search
      window.location.assign @options.baseUrl  + "/api/oauth2/authorize?response_type=token&client_id=" + encodeURIComponent(@options.clientId) + "&redirect_uri=" + encodeURIComponent(window.location.origin) + "&state=" + encodeURIComponent(state)

    else
      throw new Error("TODO")
      # TODO handle server-side JS


  signOut: =>
    # TODO revoke token
    @_deauthenticate()
    @trigger "signout"


module.exports = RadioKitAuthEditorOAuth2
