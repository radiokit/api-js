"use strict"

RadioKitBase = require('../../base')

class RadioKitAuthEditorBase extends RadioKitBase
  EVENTS: [ "success", "failure", "signout" ]

  constructor: ->
    @__signedIn = false
    @__headers = {}
    @__username = null
    @__password = null


  getHeaders: =>
    throw new Error("Not authenticated") if @isSignedIn() == false
    @__headers


  getUsername: =>
    throw new Error("Not authenticated") if @isSignedIn() == false
    @__username


  getPassword: =>
    throw new Error("Not authenticated") if @isSignedIn() == false
    @__username


  isSignedIn: =>
    @__signedIn


  signIn: =>
    # Override this method


  signOut: =>
    # Override this method


  _authenticate: (headers, username, password) =>
    throw new Error("Already authenticated") if @isSignedIn() == true
    throw new TypeError("Headers must be an object") unless typeof(headers) == "object"
    throw new TypeError("Username (if present) must be a string") unless typeof(username) == "string" unless typeof(username) == "undefined"
    throw new TypeError("Password (if present) must be a string") unless typeof(password) == "string" unless typeof(password) == "undefined"

    @__signedIn = true
    @__headers = headers
    @__username = username
    @__password = password


  _deauthenticate: =>
    throw new Error("Not authenticated") if @isSignedIn() == false
    @__signedIn = false
    @__headers = {}
    @__username = null
    @__password = null


module.exports = RadioKitAuthEditorBase
