"use strict"

class RadioKitHelpersLocation
  @parseHash: (hash) =>
    throw new TypeError("Argument passed to parseHash must be a string") unless typeof(hash) == "string"

    r = {}

    hashParams = hash.split('&')

    for hashParam in hashParams
      paramSplitted = hashParam.split('=', 2)

      if paramSplitted.length == 2
        paramSplittedKey = paramSplitted[0]
        paramSplittedKey = paramSplittedKey.substr(1) if paramSplittedKey[0] == "#"

        r[paramSplittedKey] = decodeURIComponent(paramSplitted[1])

    r


module.exports = RadioKitHelpersLocation
