"use strict"

class RadioKitHelpersString
  @underscore: (string) => 
    throw new TypeError("Argument passed to underscore must be a string") unless typeof(string) == "string"
    
    if string[0].toLowerCase() == string[0]
      underscored = string.replace(/([A-Z])/g, (_, c) -> "_#{c.toLowerCase()}")
    else
      underscored = string.replace(/([A-Z])/g, (_, c) -> "_#{c.toLowerCase()}").substr(1)

    underscored.replace(/(::_)/g, "::").replace(/(::)/g, "/")


  @modelNameToUrl: (string) => 
    throw new TypeError("Argument passed to underscore must be a string") unless typeof(string) == "string"

    string.toLowerCase().replace(/\./g, "/")
    

module.exports = RadioKitHelpersString