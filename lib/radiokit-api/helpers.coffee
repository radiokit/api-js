"use strict"

HelpersString = require('./helpers/string')
HelpersLocation = require('./helpers/location')

module.exports = {
  "String": HelpersString,
  "Location": HelpersLocation
}
