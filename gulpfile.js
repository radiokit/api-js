var fs             = require('fs');
var gulp           = require('gulp');
var watchify       = require('gulp-watchify');
var streamify      = require('gulp-streamify');
var uglify         = require('gulp-uglify');
var extReplace     = require('gulp-ext-replace');
var coffee         = require('gulp-coffee');
var jasmine        = require('gulp-jasmine');
var clean          = require('gulp-clean');

var packageVersion = JSON.parse(fs.readFileSync('./package.json')).version;

// ##### SOURCE #####

gulp.task('browserifyLib', watchify(function(watchify) {
    return gulp.src('lib/**/radiokit-api.coffee')
        .pipe(watchify({
            watch: watching,
            extensions: ['.coffee'],
            debug: false,
            setup: function(bundle) {
              bundle.transform(require('coffeeify'));
            }
        }))
        // .pipe(streamify(uglify()))
        .pipe(extReplace('.js'))
        .pipe(gulp.dest('browser/' + packageVersion + '/'));
}));

gulp.task('compileLib', function() {
    return gulp.src('lib/**/**.coffee')
      .pipe(coffee())
      .pipe(gulp.dest('build/' + packageVersion + '/'));
});

gulp.task('buildLib', ['compileLib', 'browserifyLib']);


// ##### TESTS #####

// Define source files
var sourcesSpecs = [
  'spec/radiokit-api/**/*_spec.coffee',
];

gulp.task('runSpecs', function () {
  return gulp.src(sourcesSpecs)
        .pipe(jasmine({
          includeStackTrace: true
        }));
});



// ##### GENERAL #####

// Hack to enable configurable watchify watching
var watching = false;
gulp.task('enable-watch-mode', function() { watching = true });
gulp.task('watchify', ['enable-watch-mode', 'build']);


// Entry points
gulp.task('build', ['runSpecs', 'buildLib']);
gulp.task('watch', ['watchify']);
gulp.task('spec', ['runSpecs']);
